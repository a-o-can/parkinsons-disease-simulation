function plotresponse(beta)
%PLOTRESPONSE  Plot the sigmoidal response function 

%   PLOTRESPONSE(BETA) plots the response function g(y) for a
%   specified slope (gain), BETA:
%
%              1 + tanh(beta * y) 
%      g(y) =  ------------------
%                      2
%
%   This form of response function is used in the 6-unit network
%   solved in SIGMNETWORK.
%
%   See also SIGMNETWORK.

%   Copyright 2003
%   Centre for Nonlinear Dynamics in Physiology and Medicine

y = -1:0.001:1;

g = 0.5*(1 + tanh(beta*y));

plot(y,g)
title(['Sigmoidal response function with \beta = 1,2...,10'],...
      'Fontsize',20)
text(-0.8,1.3,['g(y)=0.5(1 + tanh(\beta y))'],'Fontsize',20)
xlabel('y','Fontsize',20)
ylabel('g(y)','Fontsize',20)
axis([-1 1 -0.5 1.5])

return
