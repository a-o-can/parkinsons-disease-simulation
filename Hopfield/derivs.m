function yprime = derivs(tt,yy)
%DERIVS    [NLDBM] Calculate derivatives in Hopfield-type network with
%          sigmoidal response.  

%   YPRIME = DERIVS(TT,YY) calculates the vector YPRIME containing the
%   derivatives as functions of TT (time t) and YY (solution y) in
%   Hopfield-type network with sigmoidal response:
%
%         dy
%         -- = - y + w*g - tau,  
%         dt  
%
%   where the response function g(y) is
%
%                  1 + tanh(beta*y)
%         g(y) = -------------------
%                        2
%
%   Global variables:
%      BETAV = scalar slope (gain) of the response function
%      TAUV = nx1 vector containing unit thresholds, tau 
%      W = nxn matrix containing the 'synaptic efficacies' of the units
%          of the network.
% 
%   Reference: R Edwards, A Beuter and L Glass. (1999) Parkinsonian
%   tremor and simplification in network dynamics.  Bulletin of
%   Mathematical Biology, 61(1): 157-177.
%
%   See also RUKA4.

%   Copyright 2003
%   Centre for Nonlinear Dynamics in Physiology and Medicine

global betav tauv w

gg = 0.5*(1+tanh(betav*yy));
yprime = -yy+w*gg-tauv;

return


