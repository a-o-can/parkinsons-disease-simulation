%% Exercise

%% Hopfield Field with a step function
% specify parameters
y0 = randn(6,1);
alpha1 = 1;
tau = -1.5*ones(6,1);

% compute the solution ys, as a function of time, ts, in arbitrary time
% units
[ts, ys] = stepnetwork(y0, alpha1, tau);
% plot the solution for the first unit (neuron)
subplot(2,2,1); plot(ts, ys(1,:)); title(['alpha = ', num2str(alpha1)]);

% lower alpha for comparison
alpha2 = 0.7;
[ts2, ys2] = stepnetwork(y0, alpha2, tau);
subplot(2,2,3); plot(ts2, ys2(1,:)); title(['alpha = ', num2str(alpha2)]);

% change the thresholds
tau2 = -0.5*[3 1 3 1 1 1];
[ts3, ys3] = stepnetwork(y0, alpha1, tau2); 
[ts4, ys4] = stepnetwork(y0, alpha2, tau2);
subplot(2,2,2); plot(ts3, ys3(1,:)); title('alpha1 and tau2 = -0.5 * [3 1 3 1 1 1]');
subplot(2,2,4); plot(ts4, ys4(1,:)); title('alpha2 and tau2 = -0.5 * [3 1 3 1 1 1]');
sgtitle('Hopfield Field with Step Function')
waitforbuttonpress;

%% Hopfield Field with a sigmoid function
figure;
title('Affect of beta from 1 to 10');
hold on
for i = 1:10
    plotresponse(i);
end
hold off
waitforbuttonpress;

% specify the parameters
y0 = randn(6,1);
alpha1 = 1;
tau = -1.5*ones(6,1);
beta1 = 10;
[ts, ysigm] = sigmnetwork(y0, alpha1, tau, beta1);
figure;
hold on
subplot(2,1,1); plot(1:length(ysigm(1,:)), ysigm(1,:)); title(['alpha = ', num2str(alpha1), ' beta = ', num2str(beta1)]);

% change alpha and beta
beta2 = 4;
alpha2 = 0.7;
[ts, ysigm2] = sigmnetwork(y0, alpha2, tau, beta2);
subplot(2,1,2); plot(1:length(ysigm2(1,:)), ysigm2(1,:)); title(['alpha = ', num2str(alpha2), ' beta = ', num2str(beta2)]);
hold off
sgtitle('Hopfield Field with Sigmoid Function')
waitforbuttonpress;

%% Comparison of the result of sigmoid vs step function
figure;
hold on;
subplot(2,1,1); plot(ts, ys(1,:)); title(['Step Function with alpha= ', num2str(alpha1)]);
subplot(2,1,2); plot(1:length(ysigm), ysigm(1,:)); title(['Sigmoid Function with alpha = ', num2str(alpha1), ' beta = ', num2str(beta1)]);
hold off;
waitforbuttonpress;
