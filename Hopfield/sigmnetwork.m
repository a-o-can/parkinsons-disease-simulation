function [tn,yn] = sigmnetwork(yinitial,alpha,tau,beta)
%SIGMNETWORK  Solve 6-unit network with sigmoidal response.

%    [TN,YN] = SIGMNETWORK(YINITIAL,ALPHA,TAU,BETA) computes solution
%    YN, as a function of time TN, to the 6-unit network (Edwards et
%    al., 1999):
%
%         dy
%         -- = - y + w*g - tau,  
%         dt  
%
%   where w is a 6x6 matrix containing the 'synaptic efficacies' of the
%   units of the network, and the response function g(y) is a sigmoid
%   function of the form 
%
%                  1 + tanh(beta*y)
%         g(y) = -------------------
%                        2
%
%   Input parameters:
%    ALPHA = scalar 'synaptic connection' parameter; decreasing alpha
%             represents weakening of synaptic efficacies 
%            ( 0 <= alpha <= 1 )
%    TAU = vector of thresholds for the units in the network
%    YINITIAL = initial value of solution y = (y1,y2, ..., y6)
%    BETA = controls slope (gain) of the response function
%
%   Reference: R Edwards, A Beuter and L Glass. (1999) Parkinsonian
%   tremor and simplification in network dynamics.  Bulletin of
%   Mathematical Biology, 61(1): 157-177.
%
%   See also DERIVS, RUKA4, PLOTRESPONSE, STEPNETWORK.
%
 
%   Copyright 2003
%   Centre for Nonlinear Dynamics in Physiology and Medicine
 
global betav tauv w

% step size
h = 0.01;
% number of steps
nt = 5000;

% number of nodes
n = 6;
% time vector
tn = zeros(1,nt);
% solutions vector
yn = zeros(n,nt);

if (nargout ~= 2)
  disp('ERROR: must have 2 output variables in the command syntax')
  disp('       eg. [ts,ys]=sigmnetwork(y0,a,t,b)')
end

if (nargin ~= 4)
  disp('ERROR: must have 4 input arguments in the command syntax')
  disp('       eg. [ts,ys]=stepnetwork(y0,a,t,b)')
end

y = yinitial(:);
if (length(y) ~= 6)
  disp('ERROR: initial y must be a 6x1 vector')
end

if (length(alpha) ~= 1)
  disp('ERROR: alpha must be a scalar')
end

if ((alpha > 1.0) || (alpha < 0))
  disp('ERROR: alpha must be between 0 and 1')
end

tau = tau(:);
tauv = tau;
if (length(tauv) ~= 6)
  disp('ERROR: tau must be a 6x1 vector')
end

betav = beta;
if (length(betav) ~= 1)
  disp('ERROR: beta must be a scalar')
end

w = -[ 0 1 0 0 0 1; ...
       0 0 0 1 0 1; ...
       0 0 0 1 alpha 0; ...
       1 0 0 0 0 1; ... 
       1 1 0 0 0 0; ...
       0 0 alpha 0 alpha 0];

gg = 0.5*(1+tanh(beta*y)); % compute the sigmoid function values

yn(:,1) = y; % yn is a 6x1 vector, same as y. 1st step is computed.

disp('Computing solution ...')

% Compute the first derivative of y with the step size h and iteration
% number nt using runge kutta 4'th order
for i = 2:nt+1 % from 2nd step to the nt+1 step, overall nt steps.
  t = (i-1)*h; % time passed from the first iteration with the step size h.
  yn(:,i) = ruka4(['derivs'],y,h,t); % calculate the i'th step                                                              <-- 
  y = yn(:,i); % save the i'th state to y to use in the calculation of i+1'th step above. i+1'th step becomes i'th step above.|
end
% the time starts from h and increases by h until nt*h time units.
tn = h:h:nt*h;
% from 6x5001 to 6x5000 matrix
% yn = yn(:,1:nt);
% the excitation is between 0-1
maxElement = max(yn, [], 'all');
minElement = min(yn, [], 'all');
if abs(maxElement) > abs(minElement)
    yn = yn/maxElement;
else
    yn = yn/minElement;
end
disp('Solution computed')
return
