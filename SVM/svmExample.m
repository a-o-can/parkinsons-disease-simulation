%% Generate 100 points uniformly distributed in the unit disk
rng(1); % For reproducibility of the results, usen the same seed for random numbers
r = sqrt(rand(100,1)); % radius as the square root of a uniform random variable
phi = 2*pi*rand(100,1); % angle phi uniformly generated in (0, 2*pi)
data1 = [r.*cos(phi), r.*sin(phi)]; % results

%% Generate 100 points uniformly distributed in the annulus
r2 = sqrt(3*rand(100,1)+1); % The radius is again proportional to a square root, this time a square root of the uniform distribution from 1 through 4.
phi2 = 2*pi*rand(100,1); % angle phi
data2 = [r2.*cos(phi2), r2.*sin(phi2)]; % data2

%% Visualize the data
%{
plot(data1(:,1), data1(:,2), 'r.', 'MarkerSize',15);
hold on
plot(data2(:,1), data2(:,2), 'b.', 'MarkerSize', 15);
% ezpolar(fun) plots the polar curve rho = fun(theta) over the default domain 0 < theta < 2π.
ezpolar(@(x) 1);
ezpolar(@(x) 2); 
axis equal
hold off
%}

%%  Prepare data for SVM
data3 = [data1;data2]; % put the data in one matrix
% make a vector of classifications
theClass = ones(200,1);
theClass(1:100) = -1;

%% SVM Classifier
svmModel = fitcsvm(data3, theClass, 'KernelFunction', 'rbf', 'BoxConstraint', Inf, 'ClassName', [-1 1]); % train the model
% predict scores over the grid
dx = 0.02;
[x1Grid, x2Grid] = meshgrid( min(data3(:,1)):dx:max(data3(:,1)), min(data3(:,2)):dx:max(data3(:,2)) );
xGrid = [x1Grid(:),x2Grid(:)];
[~,scores] = predict(svmModel,xGrid);
% Plot the data and the decision boundary
figure;
h(1:2) = gscatter(data3(:,1), data3(:,2), theClass, 'rb', '.');
hold on
ezpolar(@(x) 1);
h(3) = plot(data3(svmModel.IsSupportVector,1),data3(svmModel.IsSupportVector,2),'ko');
contour(x1Grid,x2Grid,reshape(scores(:,2),size(x1Grid)),[0 0],'k');
legend(h,{'-1','+1','Support Vectors'});
axis equal
hold off