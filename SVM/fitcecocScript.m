% Source: https://de.mathworks.com/help/stats/fitcecoc.html
%% Train Multiclass Model Using SVM Learners
load fisheriris
X = meas; % predictor data
Y = species; % response data
Mdl = fitcecoc(X,Y); % train multiclass ECOC (error-correcting output codes) using the default options
Mdl.ClassNames; % display class names
CodingMat = Mdl.CodingMatrix; % columns correspond to learners, rows to classes
Mdl.BinaryLearners{1}; % the first binary learner
error = resubLoss(Mdl); % compute the resubstitution classification error

%% Train Multiclass Linear Classification Model
load nlpdata
t = templateLinear(); % create a default linear-classification-model template
X = X'; % for faster training
rng(1); % for reproducibility
Mdl = fitcecoc(X,Y, 'Learners', t, 'ObservationsIn', 'columns');