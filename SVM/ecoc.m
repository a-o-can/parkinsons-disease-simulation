d = 50;  % Height and width of the images in pixels
n = 5e4; % Sample size
tic
X = zeros(n,d^2); % Predictor matrix preallocation 
Y = zeros(n,1);   % Label preallocation
theta = 0:(1/d):(2*pi);
r = 5;            % Circle radius
rng(1);           % For reproducibility

for j = 1:n
    figmat = zeros(d);                       % Empty image
    c = datasample((r + 1):(d - r - 1),2);   % Random circle center
    x = r*cos(theta) + c(1);                 % Make the circle 
    y = r*sin(theta) + c(2);               
    idx = sub2ind([d d],round(y),round(x));  % Convert to linear indexing
    figmat(idx) = 1;                         % Draw the circle
    X(j,:) = figmat(:);                      % Store the data
    Y(j) = (c(2) >= floor(d/2)) + 2*(c(2) < floor(d/2)) + (c(1) < floor(d/2)) + 2*((c(1) >= floor(d/2)) & (c(2) < floor(d/2))); % Determine the quadrant
end

% Plot
figure
imagesc(figmat)
h = gca;
h.YDir = 'normal';
title(sprintf('Quadrant %d',Y(end)))

% Train ECOC Model
p = 0.25; % holdout sample
CVP = cvpartition(Y, 'Holdout', p); % Cross-validation data partition
isIdx = training(CVP); % Training sample indices
oosIdx = test(CVP); % Test sample indices

% SVM Model
t = templateSVM(); % template
MdlSV = fitcecoc(X(isIdx,:),Y(isIdx),'Learners',t); % pass the template and the training data to train the model
isLoss = resubLoss(MdlSV); % sample classification error

CMdl = compact(MdlSV);
toc