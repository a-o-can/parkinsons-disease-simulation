function w = hebb(nn,sig,dx)
    % self organization of symmetric cann interactions
    lrate=1; % learning rate
    w=zeros(nn);
    %%%%%%% learning session
    for epoch=1:1
    %w2(epoch,:)=w(50,:);
        for loc=1:nn
            r=in_signal_pbc(loc*dx,1,sig,nn,dx);
            dw=lrate.*r*r';
            w=w+dw;
        end
    end
    w=w*dx;
return
Function rnn_ode_u.m:
function udot=rnn(t,u,flag,nn,dx,w,I_ext)
    % odefile for recurrent network
    tau_inv = 1./1; % inverse of membrane time constant
    r=f1(u);
    sum=w*r*dx+I_ext;
    udot=tau_inv*(-u+sum+I_ext);
return