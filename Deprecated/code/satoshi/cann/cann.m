%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1-d Continuous Attractor Neural Network with Hebbian learning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clf; hold on;
nn = 100; dx=2*pi/nn; % number of nodes and resolution in deg
%weight matrices
sig = 2*pi/40;
w_sym=hebb(nn,sig,dx)/(sqrt(pi)*sig);
C=0.2;
Aw=100;
w=Aw*(w_sym-C);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Experiment %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1%%% external input to initiate bubble
u0 = zeros(nn,1)-10;
I_ext=zeros(nn,1); for i=-5:5; I_ext(nn/2+i)=10; end
param=0;
tspan=[0,10];
[t,u]=ode45('rnn_ode_u',tspan,u0,[],nn,dx,w,I_ext);
r=f1(u);
surf(t',1:nn,r','linestyle','none'); view(0,90);
%2%%% no external input to equilibrate
u0 = u(size(t,1),:);
I_ext=zeros(nn,1);
param=0;
tspan=[10,100];
[t,u]=ode45('rnn_ode_u',tspan,u0,[],nn,dx,w,I_ext);
r=f1(u);
surf(t',1:nn,r','linestyle','none');