%% Supervised training of the weight from PFC to MC. The training is
% initiated only when the are reaches the target. i.e. the end effector is
% within a small radious, ksi=0.1units, of the target location
%% Inputs:
% G_targ: the activity in MC that drives the arm to the target location
% G_PFC: the activity that PFC activation induces in MC
% pfcActivation: activation pf neurons in PFC caused by the target vector
% mu_PFC_MC: learning rate
% endEffector: position of the end effector
% targetEffector: targeted position
% ksi: radius of the difference between endEffector and targetEffector
%% Example values
% mu_PFC_MC = 0.1
%% Function
function W_PFC_MC = PFC_to_MC(G_targ, G_PFC, pfcActivation, mu_PFC_MC, endEffector, targetEffector, ksi)
    % find the number of time steps
    nTimeSteps = size(G_targ,2);
    for i = 1:nTimeSteps
        % if the end effector is near the target effector
        if abs(endEffector-targetEffector) < ksi
            % compute the change in weigths 
            delta_W_PFC_MC = mu_PFC_MC*(G_targ(:,i)-G_PFC(:,i))*pfcActivation(:,i);
            % update the weigth matrix
            W_PFC_MC = W_PFC_MC + delta_W_PFC_MC;
        % if the end effector is far from the target effector, ignore
        else
            continue
        end
    end
end