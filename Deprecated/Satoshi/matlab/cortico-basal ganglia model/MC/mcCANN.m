%% CANN part of MC
%% Inputs:
% nn: number of neurons
% A_lat: weight strength/ learning rate
% sigma: standard deviation of the weight matrix
% K: inhibition constant
% tau: firing rate of the neurons(?)
% b_MC: (?)
% G0: initial output activity of MC
% A_PC: weight strength of the output of PC
% A_BG: weight strength of the output of BG
% A_PFC: weight strength of the output of PFC
% G_PC: output of PC
% G_BG: output of BG
% G_PFC: output of PFC
%% Output:
% mc.G = output activity of motor cortex
% mc.wCANN = weigth matrix of the CANN
%% Example: mc = mcCANN(15, 10, 2, 0.5, 0.005, 0.5, rand(15,1), 0.1, rand(15,15), 1, rand(15,15), 0.1, rand(15,15));

%% Function
function mc = mcCANN(nn, A_lat, sigma, K, tau, b_MC, G0, A_PC, G_PC, A_BG, G_BG, A_PFC, G_PFC)
    % Resolution of the feature space
    dx = 2*pi/nn;
    %% Training weigth matrix
    pat = zeros(nn, nn);
    for loc = 1:nn
        i = (1:nn)';
        % periodic boundries
        dis = min(abs(i-loc), nn - abs(i-loc));
        % Pattern matrix contains nn patterns, each is a Gaussian centred
        % around one of the nodes in the network
        pat(:,loc) = exp(-(dis*dx).^2/(2*sigma^2));
    end
    
    %% Hebbian learning 
    w = pat*pat';
    % Normalizing the weight matrix, each diagonal element is equal and
    % maximal
    w = w/w(1,1);
    % Shift the weigth matrix by the inhibition constant and scale up with
    % a factor of learning rate
    w = A_lat*(w-K);
    
    %% update with input
    % g0 = nn x 1 vector of random numbers
    % time span = [0 10]
    % g is the internal state of the MC neurons
    [t, g] = ode45(@(t,g) mcActivationFunction(t, g, tau, w, G0, A_PC, G_PC, A_BG, G_BG, A_PFC, G_PFC), [0 10], rand(nn,1));
    % The output activity of the MC (G(t)) is obtained by performing a divisive
    % normalization 
    G = g.^2/(1+((2*pi)/(nn^2))*b_MC*sum(g.^2, 'all'));
    
    %% plotting the result
    subplot(1,2,1);
    plot(t,g);
    subplot(1,2,2);
    surf(g)
    % saving the output
    mc.wCANN = w;
    mc.outputActivity = G;
end