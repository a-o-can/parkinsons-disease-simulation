tic; clear; close all; clc;
%% GLOBAL VARIABLES
% timing
global T
global Ts
global sample_n

% general variables
global z
global musclesNames
global statesNames
global ys
global thisStateArray

global controlError
global controlTime
global controlIteration
global integralError

global ang_flex
global ang_pro

global u_ANC
global u_BIClong
global u_BICshort
global u_BRA
global u_BRD
global u_PT
global u_TRIlat
global u_TRIlong
global u_TRImed

global x

fpath.main = '/Users/alioguzcan/Documents/BachelorArbeit/pd_simulation_git/Satoshi_v2/matlab/opensim';
%fpath.main = 'C:\Users\SE\Desktop\gitlab\Parkinsons research\pd_simulation_alioguz_can\Satoshi_v2\matlab\opensim';
fpath.model = '../../model/PD_sim_reduced.osim';
%fpath.model = '..\..\model\PD_sim_simple.osim';
%   fpath.model = '/Users/SE/Desktop/FES_Cycling_2017/Model/FESCyclingModel2.osim';
%   fpath.model = 'C:\Users\SE\Desktop\FES_Cycling_2017\Model\FESCyclingModel2.osim';
fpath.geometry = '../../model/Geometry';
%fpath.geometry = '..\..\model\Geometry';
fpath.save = '../../results/opensim';
%fpath.save = '..\..\results\opensim';

cd(fpath.main);

import org.opensim.modeling.*
addpath('OpensimLibrary');
ModelVisualizer.addDirToGeometrySearchPaths(fpath.geometry);

%% simulation parameters
T = 1; % simulation time (sec)
Ts = 0.01;
sample_n = round(T/Ts); % sample frame size

%% set up holders
z = 1e-3;
controlError = zeros(1,sample_n);
controlTime = zeros(1,sample_n);
u_ANC = 0;
u_BIClong = 0;
u_BICshort = 0;
u_BRA = 0;
u_BRD = 0;
u_PT = 0;
u_TRIlat = 0;
u_TRIlong = 0;
u_TRImed = 0;

controlIteration = 1;
integralError = 0;

%% INITIALIZE MODEL
arm_model = Model(fpath.model);
arm_model.setUseVisualizer(1)
arm_state = arm_model.initSystem();
arm_model.equilibrateMuscles(arm_state);
thisStateArray = arm_model.getStateVariableValues(arm_state);

% get muscles and states names
musclesNames = get_muscles_names(arm_model);
statesNames = get_states_names(arm_model);

controlFunctionHandle = @feedforward;

%% Get tremor data
% get hopfield tremor data
yinitial = [thisStateArray.get(90); ... % initial activation of ANC
            thisStateArray.get(92); ... % initial activation of BIClong
            thisStateArray.get(93); ... % initial activation of BICshort
            thisStateArray.get(96); ... % initial activation of BRA
            thisStateArray.get(98); ... % initial activation of BRD
            thisStateArray.get(100);... % initial activation of PT
            thisStateArray.get(86); ... % initial activation of TRIlat
            thisStateArray.get(85); ... % initial activation of TRIlong
            thisStateArray.get(88); ... % initial activation of TRImed
            ];   
alpha = 1;
tau = -1.5*ones(9,1);
beta = 10;
[ts, ys] = sigmnetwork9(yinitial, alpha, tau, beta);


%% run simulation
tic;
timeSpan = [0 T]; 
integratorName = 'ode45'; 
integratorOptions = odeset('AbsTol', 1E-5','MaxStep',.1*Ts);

motionData = IntegrateOpenSimPlant(arm_model, controlFunctionHandle, timeSpan, ...
    integratorName, integratorOptions); % run simulation

simTime = toc();
fprintf('Simulation time = %d minutes.\n', round(simTime/60));

%% Save file
% Nsamples = length(motionData.data(:,1));
% Nstates = length(statesNames);

% str = strjoin(statesNames,'\t');
% header = ['PD_simulation \nversion=1 \nnRows=' num2str(Nsamples) ' \nnColumns=' num2str(Nstates+1) '\ninDegrees=no \nendheader \ntime	' str '\n'];
% fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'wt');
% fprintf(fid,header);
% fclose(fid);
% 
% fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'a+');
% for i = 1:size(motionData.data,1)
%     fprintf(fid,'\t%f',motionData.data(i,:)); fprintf(fid,'\n');
% end
% fclose(fid);
x2 = [linspace(0,T,size(x,1))',x];
[Nsamples,Nstates] = size(x2);

str = strjoin(statesNames,'\t');
header = ['PD_simulation \nversion=1 \nnRows=' num2str(Nsamples) ' \nnColumns=' num2str(Nstates) '\ninDegrees=no \nendheader \ntime	' str '\n'];
fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'wt');
fprintf(fid,header);
fclose(fid);

fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'a+');
for i = 1:Nsamples
    fprintf(fid,'\t%f',x2(i,:)); fprintf(fid,'\n');
end
fclose(fid);

%% Plot the muscle activation
plot(1:length(ys), ys);
legend('1: ANC', '2: BIClong', '3: BICshort', '4: BRA', '5: BRD', '6: PT', '7: TRIlat', '8: TRIlong', '9: TRImed');
userInput = 'Type in a number from 1 to 9 to see the time series of the corresponding muscle: ';
a = input(userInput);
plot(1:length(ys(a,:)), ys(a,:));
if a == 1
    legend('ANC');
elseif a == 2
    legend('BIClong');
elseif a == 3
    legend('BICshort');
elseif a == 4
    legend('BRA');
elseif a == 5
    legend('BRD');
elseif a == 6
    legend('PT');
elseif a == 7
    legend('TRIlat');
elseif a == 8
    legend('TRIlong');
elseif a == 9
    legend('TRImed');
end