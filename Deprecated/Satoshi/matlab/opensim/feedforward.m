function modelControls = feedforward(osimModel, osimState)
import org.opensim.modeling.*;

%% GLOBAL VARIABLES
% timing
global Ts
global sample_n

% general variables
global z % auxiliary continuous state variables
global musclesNames
global ys
global thisStateArray
%

global ang_flex
global ang_pro
global u_ANC
global u_BIClong
global u_BICshort
global u_BRA
global u_BRD
global u_PT
global u_TRIlat
global u_TRIlong
global u_TRImed

global controlTime
global controlIteration
global x
%% GET INFO
modelControls = osimModel.updControls(osimState);
thisTime = osimState.getTime();
thisStateArray = osimModel.getStateVariableValues(osimState);

% rep{1} = '/jointset/elbow/elbow_flexion/value';
% rep{2} = '/jointset/radioulnar/pro_sup/value';
% rep{3} = '/forceset/ANC/fiber_length';
% rep{4} = '/forceset/BIClong/fiber_length';
% rep{5} = '/forceset/BICshort/fiber_length';
% rep{6} = '/forceset/BRA/fiber_length';
% rep{7} = '/forceset/BRD/fiber_length';
% rep{8} = '/forceset/PT/fiber_length';
% rep{9} = '/forceset/TRIlat/fiber_length';
% rep{10} = '/forceset/TRIlong/fiber_length';
% rep{11} = '/forceset/TRImed/fiber_length';
%
% n_state = size(osimModel.getStateVariableNames);
% for i = 1:length(rep)
%     for j = 0:n_state-1
%         c_state  = osimModel.getStateVariableNames.get(j);
%         if strcmp(c_state,rep{i})
%             c_rep(i) = j;
%         end
%     end
% end


%% UPDATE CONTROL (only update if sampling period has passed)
if (thisTime - controlTime(controlIteration)) >= (Ts-.01*Ts)
    
    % print simulation evolution
    fprintf('%d/%d\n',controlIteration, sample_n);
    
    controlIteration = controlIteration + 1;
    controlTime(controlIteration) = thisTime;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % CONTROLLER CODE HERE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    m.elbow_flex = thisStateArray.get(74);
    m.elbow_pro = thisStateArray.get(76);
    
    m.ANC = thisStateArray.get(125); %fiber length
    m.BIClong = thisStateArray.get(127);
    m.BICshort =  thisStateArray.get(129);
    m.BRA = thisStateArray.get(131);
    m.BRD = thisStateArray.get(133);
    m.PT = thisStateArray.get(135);
    m.TRIlat = thisStateArray.get(121);
    m.TRIlong = thisStateArray.get(119);
    m.TRImed = thisStateArray.get(123);
    
    u_ANC = abs(ys(1,:));
    u_BIClong= abs(ys(2,:));
    u_BICshort = abs(ys(3,:));
    u_BRA = abs(ys(4,:));
    u_BRD = abs(ys(5,:));
    u_PT = abs(ys(6,:));
    u_TRIlat = abs(ys(7,:));
    u_TRIlong = abs(ys(8,:)/10);
    u_TRImed = abs(ys(9,:));
    
    for i = 1:osimState.getNY
        x(controlIteration,i) = thisStateArray.get(i-1);
    end

    ang_flex(controlIteration) =  m.elbow_flex;
    ang_pro(controlIteration) =  m.elbow_pro;
    
end

% assing excitation to muscles
for i = 1:length(musclesNames)
    if strcmp(musclesNames{i},'ANC')
        thisExcitation = Vector(1, u_ANC(controlIteration));
    elseif strcmp(musclesNames{i},'BIClong')
        thisExcitation = Vector(1, u_BIClong(controlIteration));
    elseif strcmp(musclesNames{i},'BICshort')
        thisExcitation = Vector(1, u_BICshort(controlIteration));
    elseif strcmp(musclesNames{i},'BRA')
        thisExcitation = Vector(1, u_BRA(controlIteration));
    elseif strcmp(musclesNames{i},'BRD')
        thisExcitation = Vector(1, u_BRD(controlIteration));
    elseif strcmp(musclesNames{i},'PT')
        thisExcitation = Vector(1, u_PT(controlIteration));
    elseif strcmp(musclesNames{i},'TRIlat')
        thisExcitation = Vector(1, u_TRIlat(controlIteration));
    elseif strcmp(musclesNames{i},'TRIlong')
        thisExcitation = Vector(1, u_TRIlong(controlIteration));
    elseif strcmp(musclesNames{i},'TRImed')
        thisExcitation = Vector(1, u_TRImed(controlIteration));
    else
        thisExcitation = Vector(1, z);
    end
    % update modelControls with the new values
    if thisExcitation ~= z
        osimModel.updActuators().get(musclesNames{i}).addInControls(thisExcitation, modelControls);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% END OF MUSCLE ENCODING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end
