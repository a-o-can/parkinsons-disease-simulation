function som = SOMSimple(ndim, nepochs, eta0, etadecay, sgm0, sgmdecay, showMode, M_L)
%SOMSimple Simple demonstration of a Self-Organizing Map that was proposed by Kohonen.
%   sommap = SOMSimple(nfeatures, ndim, nepochs, ntrainingvectors, eta0, neta, sgm0, nsgm, showMode) 
%   trains a self-organizing map with the following parameters
%       nfeatures        - dimension size of the training feature vectors
%       ndim             - width of a square SOM map
%       nepochs          - number of epochs used for training
%       ntrainingvectors - number of training vectors that are randomly generated
%       eta0             - initial learning rate
%       etadecay         - exponential decay rate of the learning rate
%       sgm0             - initial variance of a Gaussian function that
%                          is used to determine the neighbours of the best 
%                          matching unit (BMU)
%       sgmdecay         - exponential decay rate of the Gaussian variance 
%       showMode         - 0: do not show output, 
%                          1: show the initially randomly generated SOM map 
%                             and the trained SOM map,
%                          2: show the trained SOM map after each update
%
%   For example: A demonstration of an SOM map that is trained by RGB values
%           
%       som = SOMSimple(3,60,10,100,0.1,0.05,20,0.05,2);
%       som = SOMSimple(15, 10, 0.5, 0.05, 0.02, 0.05, 2, M_L);
%       % It uses:
%       %   3    : dimensions for training vectors, such as RGB values
%       %   60x60: neurons
%       %   10   : epochs
%       %   100  : training vectors
%       %   0.1  : initial learning rate
%       %   0.05 : exponential decay rate of the learning rate
%       %   20   : initial Gaussian variance
%       %   0.05 : exponential decay rate of the Gaussian variance
%       %   2    : Display the som map after every update
nrows = ndim;
ncols = ndim;
nfeatures = size(M_L,2);
ntrainingvectors = size(M_L,1);
som = rand(nrows,ncols,nfeatures)*0.5;  

tAxis = linspace(0,1,ndim);
% plot initial conditions
for i = 1:nfeatures
    plot(1:length(M_L(:,i)), M_L(:,i));
    plot(tAxis, som(:,:,i), 'or');
    plot(tAxis, som(:,:,i), 'k', 'Linewidth', 2);
    plot(tAxis', som(:,:,i)', 'k', 'Linewidth', 2);
    title('Initial Conditions')
end
 

% Generate random training data <--- M_L (muscle length vector is to be replaced by this.)
trainingData = M_L;
% Generate coordinate system
[x, y] = meshgrid(1:ncols,1:nrows);
for t = 1:nepochs    
    % Compute the learning rate for the current epoch
    eta = eta0 * exp(-t*etadecay);        
    % Compute the variance of the Gaussian (Neighbourhood) function for the current epoch
    sgm = sgm0 * exp(-t*sgmdecay);
    
    % Consider the width of the Gaussian function as 3 sigma
    width = ceil(sgm*3);        
    
    for ntraining = 1:ntrainingvectors
        % Get current training vector
        trainingVector = trainingData(ntraining,:);
                
        % Compute the Euclidean distance between the training vector and
        % each neuron in the SOM map
        dist = getEuclideanDistance(trainingVector, som, nrows, ncols, nfeatures);
        
        % Find the best matching unit (bmu)
        [~, bmuindex] = min(dist);
        
        % transform the bmu index into 2D
        [bmurow, bmucol] = ind2sub([nrows ncols],bmuindex);        
                
        % Generate a Gaussian function centered on the location of the bmu
        g = exp(-(((x - bmucol).^2) + ((y - bmurow).^2)) / (sgm*sgm));
                        
        % Determine the boundary of the local neighbourhood
        fromrow = max(1,bmurow - width);
        torow   = min(bmurow + width,nrows);
        fromcol = max(1,bmucol - width);
        tocol   = min(bmucol + width,ncols);
        % Get the neighbouring neurons and determine the size of the neighbourhood
        neighbourNeurons = som(fromrow:torow,fromcol:tocol,:);
        sz = size(neighbourNeurons);
        
        % Transform the training vector and the Gaussian function into 
        % multi-dimensional to facilitate the computation of the neuron weights update
        T = reshape(repmat(trainingVector,sz(1)*sz(2),1),sz(1),sz(2),nfeatures);                   
        G = repmat(g(fromrow:torow,fromcol:tocol),[1 1 nfeatures]);
        
        % Update the weights of the neurons that are in the neighbourhood of the bmu
        neighbourNeurons = neighbourNeurons + eta .* G .* (T - neighbourNeurons);
        % Put the new weights of the BMU neighbouring neurons back to the
        % entire SOM map
        som(fromrow:torow,fromcol:tocol,:) = neighbourNeurons;
        for i = 1:nfeatures
            plot(1:length(M_L(:,i)), M_L(:,i));
            hold on
            plot(tAxis, som(:,:,i), 'or');
            plot(tAxis, som(:,:,i), 'k', 'Linewidth', 2);
            plot(tAxis', som(:,:,i)', 'k', 'Linewidth', 2);
            title(['Epoch: ',num2str(t),'/',num2str(nepochs),', Training Vector: ',num2str(ntraining),'/',num2str(ntrainingvectors)])
            hold off
        end  
    end
end

function ed = getEuclideanDistance(trainingVector, sommap, nrows, ncols, nfeatures)
% Transform the 3D representation of neurons into 2D
neuronList = reshape(sommap,nrows*ncols,nfeatures);
% Initialize Euclidean Distance
ed = 0;
% for over nfeatures
for n = 1:size(neuronList,2) 
    % each data in training vector is substracted from each neuron of a corresponding feature
    ed = ed + (trainingVector(n)-neuronList(:,n)).^2; 
end
ed = sqrt(ed);


% Source: https://www.mathworks.com/matlabcentral/fileexchange/39930-self-organizing-map-simple-demonstration
% Author: George Azzopardi.
% Date Added: 14.06.2021, 11:44.