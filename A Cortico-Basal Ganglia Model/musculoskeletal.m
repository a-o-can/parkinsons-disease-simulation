global T
global Ts
global sample_n

% general variables
global z
global musclesNames
global statesNames

global controlError
global controlTime
global controlIteration
global integralError

global ang_flex
global ang_pro

global u_ANC
global u_BIClong
global u_BICshort
global u_BRA
global u_BRD
global u_PT
global u_TRIlat
global u_TRIlong
global u_TRImed

global x
global phiMN % muscle activation from cortico-basal ganglia model

fpath.main = '/Users/alioguzcan/Documents/BachelorArbeit/pd_simulation_git/Copy_of_A Cortico-Basal Ganglia Model_SE';
fpath.model = '/Users/alioguzcan/Documents/BachelorArbeit/pd_simulation_git/Copy_of_A Cortico-Basal Ganglia Model_SE/model/PD_sim_reduced.osim';
%  fpath.model = 'C:\Users\SE\Desktop\FES_Cycling_2017\Model\FESCyclingModel2.osim';
fpath.geometry = '/Users/alioguzcan/Documents/BachelorArbeit/pd_simulation_git/Copy_of_A Cortico-Basal Ganglia Model_SE/model/Geometry';
fpath.save = '/Users/alioguzcan/Documents/BachelorArbeit/pd_simulation_git/Copy_of_A Cortico-Basal Ganglia Model_SE/results/opensim';

cd(fpath.main);

import org.opensim.modeling.*
addpath('OpensimLibrary');
ModelVisualizer.addDirToGeometrySearchPaths(fpath.geometry);

%% simulation parameters
T = 1; % simulation time (sec)
Ts = 0.005;
sample_n = round(T/Ts); % sample frame size

%% set up holders
z = 1e-3;
controlError = zeros(1,sample_n);
controlTime = zeros(1,sample_n);
u_ANC = 0;
u_BIClong = 0;
u_BICshort = 0;
u_BRA = 0;
u_BRD = 0;
u_PT = 0;
u_TRIlat = 0;
u_TRIlong = 0;
u_TRImed = 0;

controlIteration = 1;
integralError = 0;

%% INITIALIZE MODEL
arm_model = Model(fpath.model);
arm_model.setUseVisualizer(1)
arm_state = arm_model.initSystem();
arm_model.equilibrateMuscles(arm_state);

% get muscles and states names
musclesNames = get_muscles_names(arm_model);
statesNames = get_states_names(arm_model);

controlFunctionHandle = @feedforwardModel;

%% run simulation
timeSpan = [0 T]; 
integratorName = 'ode15s'; 
integratorOptions = odeset('AbsTol', 1E-5','MaxStep',.1*Ts);

motionData = IntegrateOpenSimPlant(arm_model, controlFunctionHandle, timeSpan, ...
    integratorName, integratorOptions); % run simulation

%% Save file
% Nsamples = length(motionData.data(:,1));
% Nstates = length(statesNames);

% str = strjoin(statesNames,'\t');
% header = ['PD_simulation \nversion=1 \nnRows=' num2str(Nsamples) ' \nnColumns=' num2str(Nstates+1) '\ninDegrees=no \nendheader \ntime	' str '\n'];
% fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'wt');
% fprintf(fid,header);
% fclose(fid);
% 
% fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'a+');
% for i = 1:size(motionData.data,1)
%     fprintf(fid,'\t%f',motionData.data(i,:)); fprintf(fid,'\n');
% end
% fclose(fid);
x2 = [linspace(0,T,size(x,1))',x];
[Nsamples,Nstates] = size(x2);

str = strjoin(statesNames,'\t');
header = ['PD_simulation \nversion=1 \nnRows=' num2str(Nsamples) ' \nnColumns=' num2str(Nstates) '\ninDegrees=no \nendheader \ntime	' str '\n'];
fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'wt');
fprintf(fid,header);
fclose(fid);

fid = fopen(fullfile(fpath.save,'PD_simulation.sto'),'a+');
for i = 1:Nsamples
    fprintf(fid,'\t%f',x2(i,:)); fprintf(fid,'\n');
end
fclose(fid);

