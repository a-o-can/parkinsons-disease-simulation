%% Main for arm model

%% Set global time constants
global iterationCount % number of loop iterations
global T % simulation time

%% Define values
% for now a random value
T = 10000;
phiMN = rand(T,4); % Tx4 matrix. Rows indicate the time.

%%  Compute joint angles
% thetaJAS, thetaJAE = 1xT vector
[thetaJAS, thetaJAE] = joint_angle_SE(phiMN);

%% Compute muscle lengths vector
as = 0.04;
bs = 0.07;
ae = 0.03;
be = 0.08;
% M_L is a Tx4 matrix. Rows indicate the time.
M_L = muscle_length(thetaJAS, thetaJAE, as, bs, ae, be, T);

%% Compute end effector position 
ls = 0.3;
le = 0.3;
% xArm = [x1Arm x2Arm] Tx2 Matrix. Rows indicate the time.
xArm = end_effector_position(ls, le, as, thetaJAS, thetaJAE, T);