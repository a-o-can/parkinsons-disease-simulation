% Input.
% phi^MN(t): four dimensional vector  which respresents the muscle 
% innervations for the agonist-agtagonist pair for both the joints
% T: simulation time
% Output.
% thetaJAS: Joint angle for shoulder
% thetaJAE: Joint angle for elbow

function [thetaJAS, thetaJAE] = joint_angle_SE(phiMN)
    thetaJAS = (phiMN(:,1) - phiMN(:,3))*pi/2 + pi/2; % 1xT vector
    thetaJAE = (phiMN(:,2) - phiMN(:,4))*pi/2 + pi/2; % 1xT vector
end

% phiMN(t) is assumed to store agonist followed by the antagonist muscle
% innervations for shoulder and elbow are respectively:
% phiMN(t) = [AgS(t), AnS(t), AgE(t), AnE(t)]