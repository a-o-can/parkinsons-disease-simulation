% End effector position is estimated.

% Input.
% ls: Length of shoulder (?)
% le: Length of elbow (?)
% thetaJAS: Angle of shoulder joint
% thetaJAE: Angle of elbow joint
% as: Starting value of the movement range of the shoulder joint (?)
% T: simulation time

% Output.
% X^arm = [X_1^arm X_2^arm]: The end effector position.

function xArm = end_effector_position(d,param)
x1Arm = (param.ls - param.as)*cos(d.thetaJAS) + param.le*cos(d.thetaJAS + d.thetaJAE);
x2Arm = (param.ls - param.as)*sin(d.thetaJAS) + param.le*sin(d.thetaJAS + d.thetaJAE);
xArm = [x1Arm, x2Arm];
end