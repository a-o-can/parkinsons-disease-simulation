% The arm covers a given set of targets in the workspace, restricted by the
% range of movements of the joints. The joint angle measures are
% subsequently used to determine the lengths (mu^E and mu^S) of each
% muscle.

% Input.
% JAS, JAE: Joint angles for shoulder and elbow
% [a_s, b_s](?): Range of movements of the shoulder joint => muscle activations
% [a_e, b_e](?): Range of movements of the elbow joint

% Output.
% M_L: four dimensional length vector

function M_L = muscle_length(d, param)
muAGS = sqrt(param.as.^2 + param.bs.^2 + 2*param.as.*param.bs*cos(d.thetaJAS));
muANS = sqrt(param.as.^2 + param.bs.^2 - 2*param.as.*param.bs*cos(d.thetaJAS));
muAGE = sqrt(param.ae.^2 + param.be.^2 + 2*param.ae.*param.be*cos(d.thetaJAE));
muANE = sqrt(param.ae.^2 + param.be.^2 - 2*param.ae.*param.be*cos(d.thetaJAE));
M_L = [muAGS, muANS, muAGE, muANE]; % Tx4 matrix
end