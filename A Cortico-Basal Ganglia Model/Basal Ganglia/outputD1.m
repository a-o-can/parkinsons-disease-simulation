%% Computing the output of D1R-expressing medium spiny neurons (MSN)
% (Eq 10.17)
%% Inputs:
% lambdaD1: gain of the direct pathway
% tD1: threshold of the direct pathway
% valueDifference: is responsible for the siwtching between direct and
% indirect pathways
% G: output of MC
%% Output:
% yD1: the output of D1R-expressing medium spiny neuron (MSN)
%% Function
function yD1 = outputD1(lamdaD1, tD1, valueDifference, G_all, param)
    temp = exp(-lamdaD1*(valueDifference - tD1));
    yD1 = zeros(size(G_all));
    for i = 1:param.T
        yD1(i,:) = G_all(i,:)/temp(i);
    end
end