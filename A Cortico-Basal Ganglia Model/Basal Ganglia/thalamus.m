function [X, Y, G] = thalamus(param, pc_som, pfc_som)
% number of nodes
nn = param.som.n^2;
% resolution in deg
dx=2*pi/nn; 
%weight matrices
w_sym=hebb(nn,param.mc.sigma,dx)/(sqrt(pi)*param.mc.sigma);
w=param.mc.A_lat*(w_sym-param.mc.K);
% Nodes are initially activated on a medium level
u0 = zeros(nn,1)-10;
I_ext = param.mc.A_lat*pc_som(:,end) + param.pfc.A_pfc*pfc_som(:,end);
%I_ext = rand(100,1)>.6; % randomly created vector containing ones and zeros
% Sovle CANN
tspan=[0,100];
tall = []; rall = [];
[t,u]=ode45('rnn_ode_u',tspan,u0,[],nn,dx,w,I_ext);
r=logistic(u);
tall = [tall; t]; rall = [rall; r];
u0 = u(size(t,1),:);
%I_ext2=zeros(nn,1);
I_ext2 = I_ext; % this gives the opportunity to drop the external input, line 19, starting from any time stamp with tspan.
tspan=[100,param.T];
[t,u]=ode45('rnn_ode_u',tspan,u0,[],nn,dx,w,I_ext2);
r=logistic(u);
tall = [tall; t]; rall = [rall; r];

% Define output to be able to plot in the main.m
X = tall';
Y = 1:nn;
G = rall;
% Plot the output
surf(X,Y,G','linestyle','none');
xlabel('Time [τ]');
ylabel('Node Index');
zlabel('Firing Rate');
title("Time Evolution of Firing Rates of Neurons in a CANN");
view(-45,45);

end