%% Computing the differential equation of the STN (Eq 10.20)
%% Inputs:
% epsilon_s: connection strength of the weigths that control lateral
% connection within STN (W_slat)
% w_gs: connection strengths between the STN and GPe
% lamda_STN: controls the slope of the sigmoid
% tau_STN: timescale of STN
% x_GPe: state variable of GPe
% mat: 15x15 matrix
% sigma_lat: spread of the lateral connections
% center: given neuron [x_center, y_center]
%% Output:
% x_STN_prime: derivative of x_STN
function x_STN_prime = xSTN_ode(epsilon_s, lamda_STN, tau_STN, w_gs, x_GPe, mat, sigma_lat, center)
    W_slat = gauss2d(mat, sigma_lat, center);
    y_STN = outputSTN(lamda_STN, x_STN);
    term1 = w_gs*x_GPe;
    term2 = epsilon_s*sum(W_slat*y_STN, 'All');
    x_STN_prime = (1/tau_STN)*(-x_STN + term1 + term2);
end