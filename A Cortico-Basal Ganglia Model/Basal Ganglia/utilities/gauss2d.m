%% Computing the lateral connections within STN and GPe, which is a 2D
% Gaussian for a given neuron xc,yc 
%% Inputs:
% mat: 15x15 matrix
% sigma_lat: spread of the lateral connections
% center: given neuron [x_center, y_center]
%% Output:
% mat: 2D Gaussian
%% Function
function mat = gauss2d(mat, sigma_lat, center)
gsize = size(mat);
[R,C] = ndgrid(1:gsize(1), 1:gsize(2));
mat = gaussC(R,C, sigma_lat, center);

function val = gaussC(x, y, sigma, center)
xc = center(1);
yc = center(2);
exponent = ((x-xc).^2 + (y-yc).^2)./(2*sigma);
val       = (exp(-exponent));