%% Computing the differential equation of the GPe (Eq 10.19)
%% Inputs:
% epsilon_g: connection strength of the weigths that control lateral
% connection within GPe (W_glat)
% w_sg: connection strengths between the STN and GPe
% lamda_STN: controls the slope of the sigmoid
% yD2: output of D2R-expressing medium spiny neuron (MSN)
% tau_GPe: timescale of GPe
% mat: 15x15 matrix
% sigma_lat: spread of the lateral connections
% center: given neuron [x_center, y_center]
%% Output:
% x_GPe_prime: derivative of x_GPe Eq. 10.19
function x_GPe_prime = xGPe_ode(epsilon_g, w_sg, lamda_STN, yD2, tau_GPe, mat, sigma_lat, center)
    W_glat = gauss2d(mat, sigma_lat, center); % Eq. 10.22
    y_STN = outputSTN(lamda_STN, x_STN);
    term1 = w_sg*y_STN;
    term2 = epsilon_g*sum(W_glat*x_GPe, 'All');
    x_GPe_prime = (1/tau_GPe)*(-x_GPe + term1 + term2 + yD2);
end