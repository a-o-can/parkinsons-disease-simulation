%% Computing value difference (Eq 10.15, Eq 10.16)
%% Inputs:
% sigma_v: spatial range over which the value function is sensitive for a
% particular target
% X_targ: the goal position of a target
% X_arm: the end effector position
% T = simulation time
%% Outputs:
% V_arm: value function, over which the output of BG performs a form of
% stochastic hill-climbing. (searching for maxima)
% valueDifference: switching between direct and indirect pathways can be
% carried out using a form of the temporal difference signal called the
% "valueDifference"
%% Function Eq. 10.15 & 10.16
function [V_arm, valueDifference] = valueComputation(sigma_v, X_targ, X_arm, T)
    delta_X = X_targ - X_arm;
    V_arm = zeros(T,1);
    for i = 1:T
        V_arm(i) = exp(-(norm(delta_X(i,:))^2)/sigma_v^2);
    end
    valueDifference = zeros(T,1);
    for i = 2:T
        valueDifference(i) = V_arm(i) - V_arm(i-1);
    end
end
