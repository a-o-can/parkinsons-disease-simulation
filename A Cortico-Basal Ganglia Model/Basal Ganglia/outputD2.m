%% Computing the output of D2R-expressing medium spiny neurons (MSN)
% (Eq 10.18)
%% Inputs:
% lambdaD2: gain of the indirect pathway
% tD2: threshold of the indirect pathway
% valueDifference: is responsible for the siwtching between direct and
% indirect pathways
% G: output of MC
%% Output:
% yD2: the output of D2R-expressing medium spiny neuron (MSN)
%% Function
function yD2 = outputD2(lamdaD2, tD2, valueDifference, G_all, param)
    temp = exp(-lamdaD2*(valueDifference - tD2));
    yD2 = zeros(size(G_all));
    for i = 1:param.T
        yD2(i,:) = G_all(i,:)/temp(i);
    end
end