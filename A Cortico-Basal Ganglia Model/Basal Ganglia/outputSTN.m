%% Computing the output of STN (Eq 10.21)
%% Inputs:
% lambda_STN: controls the slope of the sigmoid
% x_STN: state variable of STN (?)
%% Output:
% ySTN: output of STN
function ySTN = outputSTN(lamda_STN, x_STN)
    ySTN = tanh(lamda_STN*x_STN);
end