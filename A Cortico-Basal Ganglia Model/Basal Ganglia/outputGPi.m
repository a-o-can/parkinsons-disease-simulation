%% Computing the output of GPi Eq. 10.23
%% Inputs:
% A_D1: coefficient of the output of D1
% y_D1: output of D1
% A_D2: coefficient of the output of STN
% y_STN: output of STN
%% Output:
% y_GPi = output of GPi
function y_GPi = outputGPi(A_D1, y_D1, A_D2, y_STN)
    y_GPi = A_D1*y_D1 - A_D2*y_STN;
end