%% Activation of the corresponding neurons in the PFC 
%% Inputs:
% xTarget = [xTarget1 xTarget2]: spatial target locations that the arm can reach in the model
% W_PFC = weight matrix of pfc 
% pfc: struct of pfc related attributes
% sigma: standard deviation of the activation function
%% Output:
% Activation of the PFC neurons
%% Example: pfc = pfcActivationFunction(xTarget, W_PFC, 0.1, pfc);
% Eq. 10.24
function U = pfcActivationFunction(xTarget, W_PFC, sigma, pfc)
    % calculate the number time steps
    nTimeSteps = size(W_PFC,2);
    % extract the Target matrices
    xTarget1 = xTarget(:,1:nTimeSteps/2);
    xTarget2 = xTarget(:,nTimeSteps/2+1:end);
    % compute and save the pfc activation
    U = exp(-((xTarget1-pfc.w1).^2+(xTarget2-pfc.w2).^2)/(sigma^2));
    % pfc.w1 & pfc.w2 are by the somPFC.m computed weight matrices of pfc 
end