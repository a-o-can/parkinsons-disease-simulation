%% This is the proprioceptive cortex existing of an N_PC x N_PC sized SOM
%% Inputs:
% n0: initial learning rate
% d0: initial radius
% iter: number of iterations
% sigma0 : effect of learning
% jointAngleShoulder: joint angle of shoulder
% jointAngleElbow: joint angle of elbow
% N_PC = size of the SOM

%% Output:
% pc: Learned SOM weight matrices
% pc.w1 = the weight matrix of the first input
% pc.w2 = the weight matrix of the second input
% pc.P_i = activation of i'th node

%% Example values:
% n0=0.5;
% d0=5;
% iter=300;
% param.sigma0 = 0.02;
% N_PC = 15;

% example call
% pc = somPC(1, 5, 300, 0.05, 15, M_L, T);
% run the 'main' script prior to this one.
%% Function
function pc = somPC(param, M_L)

% Create the weight matrices
w1 = zeros(param.som.n ,param.som.n);
w2 = w1;

% Initialize the weight vectors
for j1=1:param.som.n 
    for j2=1:param.som.n 
        w1(j1,j2)=rand*(0.05);
        w2(j1,j2)=rand*(0.05);
    end
end


for i = 1:param.T
    % Defining the input data
    x1 = [M_L(i, 1) M_L(i, 3)]; % agonist muscles for Shoulder/Elbow
    x2 = [M_L(i, 2) M_L(i, 4)]; % antagonist muscles for Shoulder/Elbow
    
    % t = 1 starting from the first iteration.
    t=1;
    n0 = 1;
    d0 = 1;
    sigma0 = 0.5;
    while (t<=param.T)
        n=n0*(1-t/param.T);
        d=round(d0*(1-t/param.T));
        % lambda: time constant
        lambda = param.T/log(d);
        sigma = sigma0*exp(-t/lambda);
        %loop for the 701 inputs
        for i=1:length(x1)
            e_norm=(x1(i)-w1).^2+(x2(i)-w2).^2;
            minj1=1;
            minj2=1;
            min_norm=e_norm(minj1,minj2);
            % find out the smallest norm of the current param.iteration
            for j1=1:param.som.n
                for j2=1:param.som.n
                    if e_norm(j1,j2)<min_norm
                        min_norm=e_norm(j1,j2);
                        minj1=j1;
                        minj2=j2;
                    end
                end
            end
            % j1star & j2star: index of the bmu for each param.iteration
            j1star= minj1;
            j2star= minj2;
            % Lateral distance between neurons
            dEucl1 = norm(x1(i)-w1(j1star,j2star));
            dEucl2 = norm(x2(i)-w2(j1star,j2star));
            % to represent the amount of influence a node's distance from the BMU has on its learning.
            influence1 = exp(-dEucl1.^2 / (sigma^2));
            influence2 = exp(-dEucl2.^2 / (sigma^2));
            %update the winning neuron
            w1(j1star,j2star)=w1(j1star,j2star)+influence1*n*(x1(i)- w1(j1star,j2star));
            w2(j1star,j2star)=w2(j1star,j2star)+influence2*n*(x2(i)- w2(j1star,j2star));
            %update the neighbour neurons
            for dd=1:1:d
                % to left
                jj1=j1star-dd;
                jj2=j2star;
                if (jj1>=1)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
                % to right
                jj1=j1star+dd;
                jj2=j2star;
                if (jj1<=10)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
                % to downwards
                jj1=j1star;
                jj2=j2star-dd;
                if (jj2>=1)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
                % to upwards
                jj1=j1star;
                jj2=j2star+dd;
                if (jj2<=10)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
            end
        end
        t=t+1;
    end
    pc.w1 = w1;
    pc.w2 = w2;
    
    % Activation of each node (Eq. 10.9)
    P1 = zeros(param.som.n);
    P2 = P1;
    for i = 1:param.som.n
        P1(i,:) = exp(-(norm(x1(i) - w1(i, :))^2) / (sigma0)^2);
        P2(i,:) = exp(-(norm(x2(i) - w2(i, :))^2) / (sigma0)^2);
    end
    
    pc.P1 = P1;
    pc.P2 = P2;
    
end
end

%% Problem:
% The model in the book uses the a 4D length vector of the
% antagonist/agonist muscles. Here, i am using the joint angles to train
% the SOM. This has to be changed.

