%% CANN part of MC
%% Inputs:
% nn: number of neurons
% A_lat: weight strength/ learning rate
% sigma: standard deviation of the weight matrix
% K: inhibition constant
% tau: firing rate of the neurons(?)
% b_MC: (?)
% G0: initial output activity of MC
% A_PC: weight strength of the output of PC
% A_BG: weight strength of the output of BG
% A_PFC: weight strength of the output of PFC
% G_PC: output of PC
% G_BG: output of BG
% G_PFC: output of PFC
% T: simulation time
%% Output:
% G = output activity of motor cortex
%% Example: G = mcCANN(15, 10, 2, 0.5, 0.005, 0.5, rand(15,1), 0.1, mc, 1, rand(15), 0.1, rand(15));

%% Function
function [G, G_all] = mcCANN(nn, A_lat, sigma, K, tau, b_MC, G, A_PC, pc_som, A_BG, G_BG, A_PFC, pfc_som, param)
    % Resolution of the feature space
    dx = 2*pi/nn;
    %% Training weigth matrix
    pat = zeros(nn, nn);
    for loc = 1:nn
        i = (1:nn)';
        % periodic boundries
        dis = min(abs(i-loc), nn - abs(i-loc));
        % Pattern matrix contains nn patterns, each is a Gaussian centred
        % around one of the nodes in the network
        pat(:,loc) = exp(-(dis*dx).^2/(2*sigma^2));
    end
    
    %% Hebbian learning 
    w = pat*pat';
    % Normalizing the weight matrix, each diagonal element is equal and
    % maximal
    w = w/w(1,1);
    % Shift the weigth matrix by the inhibition constant and scale up with
    % a factor of learning rate
    w = A_lat*w-K;
    
    %% update with input
    % g0 = nn x 1 vector of random numbers
    % time span = [0 T]
    % g is the internal state of the MC neurons (Eq. 10.11)
    %g = zeros(nn^2, param.T);
    %W = zeros(param.mc.n);
% Computate the kernel
    %for i = 1:param.mc.n
    %    for j = 1:param.mc.n
    %        W = gauss2d(W, 0.5, [i,j]);
    %    end
    %end
    I_MC = A_PC*pc_som + A_BG*G_BG + A_PFC*pfc_som; % [param.pc.n x T]
    M = kron(w,G);
    func = @(g, I_MC) (tau^-1)*(-g + M + I_MC);
    G_all = zeros(param.mc.n^2, param.mc.n^2, param.T);
    integralMin = 0;
    for i = 1:param.T
        integralMax = i;
        g = integral(@(g) func(g, I_MC(:,i)), integralMin, integralMax, 'ArrayValued', true);
        % The output activity of the MC (G(t)) is obtained by performing a divisive
        % normalization 
        G = g.^2/(1+((2*pi)/(nn^2))*b_MC*sum(g.^2, 'all')); %(Eq. 10.12)
        G_all(:,:,i) = G;
    end
    %% plotting the result
    [X, Y] = meshgrid(linspace(min(G,[],'all'),max(G,[],'all'),size(G,1)));
    %C = X.*Y;
    surf(X,Y,G);
    %colorbar;
    % view(90,0);
end