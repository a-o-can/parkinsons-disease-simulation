% Main program
hold on;
% number of nodes^
nn = 100;
% resolution in deg
dx=2*pi/nn; 
%weight matrices
sig = 2*pi/40;
w_sym=hebb(nn,sig,dx)/(sqrt(pi)*sig);
C=0.2;
Aw=100;
w=Aw*(w_sym-C);

%%% Experiment %%%

%1%%% external input to initiate bubble
u0 = zeros(nn,1)-10;
I_ext=zeros(nn,1);
for i=-5:5
    I_ext(nn/2+i)=10;
end


tspan=[0,10];
tall = []; rall = [];
[t,u]=ode45('rnn_ode_u',tspan,u0,[],nn,dx,w,I_ext);
r=logistic(u);
tall = [tall; t]; rall = [rall; r];
u0 = u(size(t,1),:);
I_ext2=zeros(nn,1);
tspan=[10,100];
[t,u]=ode45('rnn_ode_u',tspan,u0,[],nn,dx,w,I_ext2);
r=logistic(u);
tall = [tall; t]; rall = [rall; r];

size(tall)
size(1:nn)
size(rall)
surf(tall',1:nn,rall','linestyle','none');
xlabel('Time [τ]');
ylabel('Node Index');
zlabel('Firing Rate');
title("Time Evolution of Firing Rates of Neurons in a CANN");
view(0,45);