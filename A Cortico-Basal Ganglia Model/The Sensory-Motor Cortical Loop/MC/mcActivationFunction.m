%% The activation dynamics of the MC
%% Inputs:
% tau = time constant
% A_PC: weight strength of the output of PC
% A_BG: weight strength of the output of BG
% A_PFC: weight strength of the output of PFC
% A_lat: modulates weight kernel
% K: inhibition constant
% G_PC: output of PC
% G_BG: output of BG
% G_PFC: output of PFC
% G0: random initial input?
% W: weight kernel gauss2d.m (Basal Ganglia/utilities): gauss2d(rand(5,5), 0.5, [2,3])
% param: saves the network constants
%% Outputs:
% Differential equation of the activation dynamics
%% Example solver
% [t, g] = ode45(@(t,g) mcActivationFunction(g, 0.05, w, G, 0.1, pc_som(:,i), 0, 0, 0, 0), [0 param.T], rand(param.som.n^2,1));
function dgdt = mcActivationFunction(g, tau, W, G, A_PC, pc_som, A_BG, G_BG, A_PFC, pfc_som, param)

    % Define input
    I_MC = A_PC*pc_som + A_BG*G_BG + A_PFC*pfc_som; % [param.pc.n x T]
    M = kron(W,G);
    dgdt = (tau^-1)*(-g + M + I_MC);
        
    fprintf('Number of neurons in SOM_PC: %d\n', sqrt(size(pc_som,1)));
    fprintf('Size of SOM_PC Input: %d x %d\n', size(pc_som,1), size(pc_som,2));
    fprintf('Size of I_MC: %d x %d\n', size(I_MC,1), size(I_MC,2));
    fprintf('Size of G0: %d x %d\n', size(G,1), size(G,2));
    fprintf('Size of W: %d x %d\n', size(W,1), size(W,2));
    fprintf('Size of kron(W,G0): %d x %d\n', size(kron(W,G),1), size(kron(W,G),2));
end