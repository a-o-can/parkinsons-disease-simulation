%% MN Layer
%% Inputs:
% A_MN: Coefficient of the projection from MC to MN
% mu_MC_MN: Learning rate of the weigth matrix
% W_MC_MN: Weigth matrix between the neurons of MC and MN
% desiredActivation: Initially provided desired activation to provoke a
% sensory activity in PC, which in return generates a motor activity in
% MC(G)
% networkActivation: The network-drived MN activation
% G: Output activity of MC
%% Output:
% phiMN: four dimensional vector  which respresents the muscle 
% innervations for the agonist-agtagonist pair for both the joints.
function phiMN = mn(A_MN, mu_MC_MN, W_MC_MN, desiredActivation, networkActivation, G)
    % Train the weigth matrix between MC and MN
    deltaW_MC_MN = mu_MC_MN*(desiredActivation - networkActivation)*G;
    % Update the weigth matrix
    W_MC_MN = W_MC_MN + deltaW_MC_MN;
    % Close the loop by computing phiMN for the arm model
    phiMN = A_MN*W_MC_MN*G;
end
