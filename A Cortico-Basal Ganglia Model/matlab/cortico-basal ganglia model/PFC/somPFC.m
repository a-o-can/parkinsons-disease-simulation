%% This is the prefrontal cortex existing of an N_PC x N_PC sized SOM
%% Inputs:
% n0: initial learning rate
% d0: initial radius
% T: number of iterations
% sigma0 : effect of learning
% N_PC: size of the SOM
% xTarget1, xTarget2: spatial target locations that the arm can reach in the model

%% Output:
% pfc.w1 = the weight matrix of the first input
% pfc.w2 = the weight matrix of the second input

%% Example values:
% n0=0.5;
% d0=5;
% T=300;
% sigma0 = 0.02;
% N_PC = 15;

% example call
% pc = somPC(1,5,300,0.05, 15, xTarget1, xTarget2);
% run the 'main' script prior to this one.
%% Function
function pfc = somPFC(n0, d0, T, sigma0, N_PC, jointAngleShoulder, jointAngleElbow)
    % Defining the input data
    x1 = jointAngleShoulder;
    x2 = jointAngleElbow;

    % Create the weight matrices
    w1 = zeros(N_PC,N_PC);
    w2 = w1;

    % Initialize the weight vectors 
    for j1=1:N_PC
        for j2=1:N_PC
            w1(j1,j2)=rand*(0.05);
            w2(j1,j2)=rand*(0.05);
        end
    end

    % plot the initial conditions
    figure(1)
    plot(x1,x2,'.b')
    hold on
    plot(w1,w2,'or')
    plot(w1,w2,'k','linewidth',2)
    plot(w1',w2','k','linewidth',2)
    hold off
    title('t=0');
    drawnow


    % t = 1 starting from the first iteration.
    t=1;
    while (t<=T)
        n=n0*(1-t/T);
        d=round(d0*(1-t/T));
        % lambda: time constant
        lambda = T/log(d);
        sigma = sigma0*exp(-t/lambda);
        %loop for the 701 inputs
        for i=1:length(x1)
            e_norm=(x1(i)-w1).^2+(x2(i)-w2).^2;
            minj1=1;
            minj2=1;
            min_norm=e_norm(minj1,minj2);
            % find out the smallest norm of the current iteration
            for j1=1:N_PC
                for j2=1:N_PC
                    if e_norm(j1,j2)<min_norm
                        min_norm=e_norm(j1,j2);
                        minj1=j1;
                        minj2=j2;
                    end
                end
            end
            % j1star & j2star: index of the bmu for each iteration
            j1star= minj1;
            j2star= minj2;
            % Lateral distance between neurons
            dEucl1 = norm(x1(i)-w1(j1star,j2star));
            dEucl2 = norm(x2(i)-w2(j1star,j2star));
            % to represent the amount of influence a node's distance from the BMU has on its learning.
            influence1 = exp(-dEucl1.^2 / (sigma^2));
            influence2 = exp(-dEucl2.^2 / (sigma^2));
            %update the winning neuron
            w1(j1star,j2star)=w1(j1star,j2star)+influence1*n*(x1(i)- w1(j1star,j2star));
            w2(j1star,j2star)=w2(j1star,j2star)+influence2*n*(x2(i)- w2(j1star,j2star));
            %update the neighbour neurons
            for dd=1:1:d
                % to left
                jj1=j1star-dd;
                jj2=j2star;
                if (jj1>=1)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
                % to right
                jj1=j1star+dd;
                jj2=j2star;
                if (jj1<=10)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
                % to downwards
                jj1=j1star;
                jj2=j2star-dd;
                if (jj2>=1)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
                % to upwards
                jj1=j1star;
                jj2=j2star+dd;
                if (jj2<=10)
                    w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                    w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
                end
            end
        end
        t=t+1;
        figure(1)
        plot(x1,x2,'.b')
        hold on
        plot(w1,w2,'or')
        plot(w1,w2,'k','linewidth',2)
        plot(w1',w2','k','linewidth',2)
        hold off
        title(['t=' num2str(t)]);
        drawnow

    end
    pfc.w1 = w1;
    pfc.w2 = w2;
end

%% Problem:
% The model in the book uses the a 4D length vector of the
% antagonist/agonist muscles. Here, i am using the joint angles to train
% the SOM. This has to be changed.