% Number of samples in each cluster
K = 200; 

% offset of classes
q = 1.1;

% define 4 clusters
P = [rand(1,K)-q rand(1,K)+q rand(1,K)-q rand(1,K)+q;
     rand(1,K)+q rand(1,K)+q rand(1,K)-q rand(1,K)-q];
 
plot(P(1,:), P(2,:), 'g.');
hold on;
grid on;

dimensions = [10, 10];
coverSteps = 10;
initNeighbor = 4;
topologyFnc = 'hextop';
distanceFnc = 'linkdist';

net2 = selforgmap(dimensions, coverSteps, initNeighbor, topologyFnc, distanceFnc);
[net2, Y] = train(net2,P);
plotsompos(net2, P);
grid on;
hold off;