% self organizing map Kohonen
% Programmed by Ammar AL-Jodah
clc
close all

% initialize the feature vectors
%for i=1:1000
%    x1(i)=rand;
%    x2(i)=rand;
%end
x1 = rand(1,1000);
x2 = rand(1,1000);

w1 = zeros(10,10);
w2 = w1;
% initialize the weight vectors 
for j1=1:10
    for j2=1:10
        w1(j1,j2)=rand*(0.52-0.48)+0.48;
        w2(j1,j2)=rand*(0.52-0.48)+0.48;
    end
end

% plot the initial conditions
figure(1)
plot(x1,x2,'.b')
hold on
plot(w1,w2,'or')
plot(w1,w2,'k','linewidth',2)
plot(w1',w2','k','linewidth',2)
hold off
title('t=0');
drawnow

d0=5;
n0 = 1;
sigma = 0.02;
% T = number of iterations
T=300;
% t = 1 starting from the first iteration.
t=1;
% effect of learning
sigma0 = 0.05;
while (t<=T)
    n=n0*(1-t/T);
    d=round(d0*(1-t/T));
    %loop for the 1000 inputs
    for i=1:1000
        e_norm=(x1(i)-w1).^2+(x2(i)-w2).^2;
        minj1=1;
        minj2=1;
        min_norm=e_norm(minj1,minj2);
        % find out the smallest norm of the current iteration
        for j1=1:10
            for j2=1:10
                if e_norm(j1,j2)<min_norm
                    min_norm=e_norm(j1,j2);
                    minj1=j1;
                    minj2=j2;
                end
            end
        end
        % j1star & j2star: index of the bmu for each iteration
        j1star= minj1;
        j2star= minj2;
        % to represent the amount of influence a node's distance from the BMU has on its learning.
        influence1 = exp(-((x1(i)-w1(j1star,j2star)).^2 / sigma*sigma));
        influence2 = exp(-((x2(i)-w2(j1star,j2star)).^2 / sigma*sigma));
        %update the winning neuron
        w1(j1star,j2star)=w1(j1star,j2star)+influence1*n*(x1(i)- w1(j1star,j2star));
        w2(j1star,j2star)=w2(j1star,j2star)+influence2*n*(x2(i)- w2(j1star,j2star));
        %update the neighbour neurons
        for dd=1:1:d
            % to left
            jj1=j1star-dd;
            jj2=j2star;
            if (jj1>=1)
                w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
            end
            % to right
            jj1=j1star+dd;
            jj2=j2star;
            if (jj1<=10)
                w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
            end
            % to downwards
            jj1=j1star;
            jj2=j2star-dd;
            if (jj2>=1)
                w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
            end
            % to upwards
            jj1=j1star;
            jj2=j2star+dd;
            if (jj2<=10)
                w1(jj1,jj2)=w1(jj1,jj2)+influence1*n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+influence2*n*(x2(i)-w2(jj1,jj2));
            end
        end
    end
    t=t+1;
    figure(1)
    plot(x1,x2,'.b')
    hold on
    plot(w1,w2,'or')
    plot(w1,w2,'k','linewidth',2)
    plot(w1',w2','k','linewidth',2)
    hold off
    title(['t=' num2str(t)]);
    drawnow
    
end


% Source: https://www.mathworks.com/matlabcentral/fileexchange/46481-self-organizing-map-kohonen-neural-network
% Author: Ammar AL-Jodah
% Date: 14/06/2021, 12:35.