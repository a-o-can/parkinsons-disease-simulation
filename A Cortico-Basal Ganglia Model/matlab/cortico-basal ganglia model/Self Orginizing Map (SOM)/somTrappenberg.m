%% SOM presented in Fundamentals of Computational Neuroscience 2nd Edition by Trappenberg, page 187
function som = somTrappenberg(nn, lambda, sigma, c1, c2)
    [X, Y] = meshgrid(1:nn, 1:nn);
    ntrial = 0;
    sig2 = 1/(sigma^2);
    % Training session
    while(true)
        if(mod(ntrial,100)==0) % Plot grid of feature centres in every 100 iterations
            clf; hold on; axis square; axis([0 1 0 1]); grid on;
            plot(c1, c2, 'k'); plot(c1', c2', 'k');
            tstring = [int2str(ntrial) ' examples']; title(tstring) 
            waitforbuttonpress;
        end
        r_in = [rand(length(c1)); rand(length(c2))];
        r = exp(-(c1-r_in(1)).^2 - (c2-r_in(2)).^2);
        [rmax, x_winner] = max(max(r)); [rmax, y_winner] = max(max(r'));
        r = exp(-((X-x_winner).^2 + (Y-y_winner).^2)*sig2);
        c1 = c1 + lambda*r.*(r_in(1)-c1);
        c2 = c2 + lambda*r.*(r_in(2)-c2);
        ntrial = ntrial+1;
        som.feature1 = c1;
        som.feature2 = c2;
    end
end