%% The activation dynamics of the MC
%% Inputs:
% A_PC: weight strength of the output of PC
% A_BG: weight strength of the output of BG
% A_PFC: weight strength of the output of PFC
% G_PC: output of PC
% G_BG: output of BG
% G_PFC: output of PFC
%% Outputs:
% Differential equation of the activation dynamics
%% Example solver
% [t, y] = ode45(@(t,y) mcActivationFunction(t, y, 1, rand(3,1), rand(3,1), rand(3,1)), [0 10], rand(3,1));
function dgdt = mcActivationFunction(t, g, tau, W, G0, A_PC, G_PC, A_BG, G_BG, A_PFC, G_PFC)
I_MC = A_PC*G_PC + A_BG*G_BG + A_PFC*G_PFC;
dgdt = (tau^-1)*(-g + W.*G0 + I_MC);
end