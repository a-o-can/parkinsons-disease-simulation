function d= generate_data(T)
muscle_activation = rand(T,4);  % Tx4 matrix muscle activation to the joint
[d.thetaJAS, d.thetaJAE] = joint_angle_SE(muscle_activation); % Compute joint angles give motor neuron activation (Eq. 10.1, 10.2)
d.muscleLength = muscle_length(d, param.arm);% Compute muscle lengths vector. % M_L is a Tx4 matrix. Rows indicate the time. (Eq. 10.3, 10.4, 10.5, 10.6)
d.pos = end_effector_position(d, param.arm); % Eq. 10.7, 10.8 % xArm = [x1Arm x2Arm] Tx2 Matrix. Rows indicate the time.
end


