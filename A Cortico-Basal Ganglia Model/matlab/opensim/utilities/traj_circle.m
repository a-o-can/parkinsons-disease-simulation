function [ref_x,ref_xd] = traj_circle(param,iz)
t = linspace(-(1/param.fs),param.time,param.n+1);
x = (sin((t+.5)*pi*2*param.ref.sin)'+1)/2*deg2rad(param.joint.lim)/2; % rad
xd = diff(x)*param.fs; % shifted by 1 frame
ref_x = x(i+1,1); %because first frame velocity not available...
ref_xd = xd(i,1);
end