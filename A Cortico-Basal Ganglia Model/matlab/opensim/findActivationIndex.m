rep{1} = '/jointset/elbow/elbow_flexion/value';
rep{2} = '/jointset/radioulnar/pro_sup/value';
rep{3} = '/forceset/ANC/activation';
rep{4} = '/forceset/BIClong/activation';
rep{5} = '/forceset/BICshort/activation';
rep{6} = '/forceset/BRA/activation';
rep{7} = '/forceset/BRD/activation';
rep{8} = '/forceset/PT/activation';
rep{9} = '/forceset/TRIlat/activation';
rep{10} = '/forceset/TRIlong/activation';
rep{11} = '/forceset/TRImed/activation';

osimModel = arm_model;

n_state = size(osimModel.getStateVariableNames);
for i = 1:length(rep)
    for j = 0:n_state-1
        c_state  = osimModel.getStateVariableNames.get(j);
        if strcmp(c_state,rep{i})
            c_repActivation(i) = j;
        end
    end
end