function modelControls = feedforward(osimModel, osimState)
import org.opensim.modeling.*;

%% GLOBAL VARIABLES
% timing
global Ts
global sample_n

% general variables
global z
global musclesNames
%

global ang_flex
global ang_pro
global u_ANC
global u_BIClong
global u_BICshort
global u_BRA
global u_BRD
global u_PT
global u_TRIlat
global u_TRIlong
global u_TRImed

global controlTime
global controlIteration
global thisStateArray
global x
global phiMN % muscle activation from cortico-basal ganglia model
%% GET INFO
modelControls = osimModel.updControls(osimState);
thisTime = osimState.getTime();
thisStateArray = osimModel.getStateVariableValues(osimState);

% rep{1} = '/jointset/elbow/elbow_flexion/value';
% rep{2} = '/jointset/radioulnar/pro_sup/value';
% rep{3} = '/forceset/ANC/fiber_length';
% rep{4} = '/forceset/BIClong/fiber_length';
% rep{5} = '/forceset/BICshort/fiber_length';
% rep{6} = '/forceset/BRA/fiber_length';
% rep{7} = '/forceset/BRD/fiber_length';
% rep{8} = '/forceset/PT/fiber_length';
% rep{9} = '/forceset/TRIlat/fiber_length';
% rep{10} = '/forceset/TRIlong/fiber_length';
% rep{11} = '/forceset/TRImed/fiber_length';
%
% n_state = size(osimModel.getStateVariableNames);
% for i = 1:length(rep)
%     for j = 0:n_state-1
%         c_state  = osimModel.getStateVariableNames.get(j);
%         if strcmp(c_state,rep{i})
%             c_rep(i) = j;
%         end
%     end
% end

%% UPDATE CONTROL (only update if sampling period has passed)
if (thisTime - controlTime(controlIteration)) >= (Ts-.01*Ts)
    
    % print simulation evolution
    fprintf('%d/%d\n',controlIteration, sample_n);
    
    controlIteration = controlIteration + 1;
    controlTime(controlIteration) = thisTime;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % CONTROLLER CODE HERE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    m.elbow_flex = thisStateArray.get(46);
    m.elbow_pro = thisStateArray.get(48);
    
    m.ANC = thisStateArray.get(91); %fiber length
    m.BIClong = thisStateArray.get(93);
    m.BICshort =  thisStateArray.get(95);
    m.BRA = thisStateArray.get(97);
    m.BRD = thisStateArray.get(99);
    m.PT = thisStateArray.get(101);
    m.TRIlat = thisStateArray.get(87);
    m.TRIlong = thisStateArray.get(85);
    m.TRImed = thisStateArray.get(89);
    
    u_ANC = phiMN(:,3);
    u_BIClong= phiMN(:,1);
    u_BICshort = phiMN(:,1);
    u_BRA = phiMN(:,3);
    u_BRD = phiMN(:,4);
    u_PT = phiMN(:,4);
    u_TRIlat = phiMN(:,2);
    u_TRIlong = phiMN(:,2);
    u_TRImed = phiMN(:,2);
    
    for i = 1:osimState.getNY
        x(controlIteration,i) = thisStateArray.get(i-1);
    end
    
    ang_flex(controlIteration) =  m.elbow_flex;
    ang_pro(controlIteration) =  m.elbow_pro;
end
% assing excitation to muscles
for i = 1:length(musclesNames)
    if strcmp(musclesNames{i},'ANC')
        thisExcitation = Vector(1, u_ANC(controlIteration));
    elseif strcmp(musclesNames{i},'BIClong')
        thisExcitation = Vector(1, u_BIClong(controlIteration));
    elseif strcmp(musclesNames{i},'BICshort')
        thisExcitation = Vector(1, u_BICshort(controlIteration));
    elseif strcmp(musclesNames{i},'BRA')
        thisExcitation = Vector(1, u_BRA(controlIteration));
    elseif strcmp(musclesNames{i},'BRD')
        thisExcitation = Vector(1, u_BRD(controlIteration));
    elseif strcmp(musclesNames{i},'PT')
        thisExcitation = Vector(1, u_PT(controlIteration));
    elseif strcmp(musclesNames{i},'TRIlat')
        thisExcitation = Vector(1, u_TRIlat(controlIteration));
    elseif strcmp(musclesNames{i},'TRIlong')
        thisExcitation = Vector(1, u_TRIlong(controlIteration));
    elseif strcmp(musclesNames{i},'TRImed')
        thisExcitation = Vector(1, u_TRImed(controlIteration));
    else
        thisExcitation = Vector(1, z);
    end
    % update modelControls with the new values
    if thisExcitation ~= z
        osimModel.updActuators().get(musclesNames{i}).addInControls(thisExcitation, modelControls);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% END OF MUSCLE ENCODING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



end
