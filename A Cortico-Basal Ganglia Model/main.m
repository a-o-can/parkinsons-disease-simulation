% Main script to run for the 2D arm model (Eq. # stands for Equation number)
global phiMN
%% setup the simulation
param.T = 400;  % for now a random value

%% Set path 
fpath.main = 'C:\Users\SE\Desktop\gitlab\Parkinsons research\pd_simulation_alioguz_can\A Cortico-Basal Ganglia Model';
fpath.loop = '/Users/alioguzcan/Documents/BachelorArbeit/pd_simulation_git/Copy_of_A Cortico-Basal Ganglia Model_SE/The Sensory-Motor Cortical Loop';
fpath.bg = '/Users/alioguzcan/Documents/BachelorArbeit/pd_simulation_git/Copy_of_A Cortico-Basal Ganglia Model_SE/Basal Ganglia';
addpath(genpath(fpath.main));
addpath(genpath(fpath.loop));
addpath(genpath(fpath.bg));

%% parameters
param.arm.as = 0.04;  % values in the book, page 183, Table 10.1
param.arm.bs = 0.07;
param.arm.ae = 0.03;
param.arm.be = 0.08;

param.arm.ls = 0.3; % shoulder length
param.arm.le = 0.3; % elbow

% param.pc.n0=0.5; % initial learning rate
% param.pc.d0=5;   % initial radius
% param.pc.iter=300;   % number of iterations
% param.pc.sigma0 = 0.02;  % effect of learning
% param.pc.N_PC = 15;  % size of the SOM
% 
% param.pfc.n0=0.5; % initial learning rate
% param.pfc.d0=5;   % initial radius
% param.pfc.iter=300;   % number of iterations
% param.pfc.sigma0 = 0.02;  % effect of learning
% param.pfc.N_PC = 15;  % size of the SOM
% 
% param.mc.n0=0.5;
% param.mc.d0=5;
% param.mc.iter=300;
% param.mc.sigma0 = 1;
% param.mc.N_MC = 15;

param.som.n = 10;
param.pfc.n = 10;
param.mc.n = 10;

param.pfc.workspace = .5; %meter
param.pfc.res = 0.1; % workspace resolution in meter
param.pfc.A_pfc = 10;

%% Arm Model
disp('Arm Model runs ...');
rng(1); % For reproducibility of the results, usen the same seed for random numbers
d.muscleActivation = rand(param.T,4);  % Tx4 matrix muscle activation to the joint 
[d.thetaJAS, d.thetaJAE] = joint_angle_SE(d.muscleActivation); % Compute joint angles give motor neuron activation (Eq. 10.1, 10.2)
d.muscleLength = muscle_length(d, param.arm);% Compute muscle lengths vector. % M_L is a Tx4 matrix. Rows indicate the time. (Eq. 10.3, 10.4, 10.5, 10.6)
d.pos = end_effector_position(d, param.arm); % Eq. 10.7, 10.8 % xArm = [x1Arm x2Arm] Tx2 Matrix. Rows indicate the time.
disp('Arm Model is computed');

%% PC model
%%% read
%%% https://www.mathworks.com/help/deeplearning/ug/neural-network-subobject-properties.html
%%% for network configuration.
disp('PC Model runs ...');
tem_som = selforgmap([param.som.n param.som.n]);
net.som = train(tem_som,d.muscleLength');
pc_som = net.som(d.muscleLength');
disp('PC Model is computed');

%% PFC Model
%%% prepare set of end-effector position to be modelled
disp('PFC Model runs ...');
%x = -param.pfc.workspace:param.pfc.res:param.pfc.workspace;
x = linspace(-param.pfc.workspace, param.pfc.workspace, param.pfc.n^2);
x_list = fullfact([size(x,2),size(x,2)]);
x_full = x(x_list);

% Run PFC for activation values (Output = U(t), will be an input for MC)
tem_pfc = selforgmap([param.pfc.n param.pfc.n]);
net.pfc = train(tem_pfc,x_full');
pfc_som = net.pfc(x_full');
disp('PFC Model is computed');

%% MC Model
% Run SOM of MC
disp('SOM of MC Model runs ...');
tem_mc = selforgmap([param.mc.n param.mc.n]);
net.mc = train(tem_mc,pc_som(:));
mc_som = net.mc(pc_som(:));
disp('SOM of MC Model is computed');
% Run CANN of MC Equation 
param.mc.A_lat = 10; % values in the book, page 183, Table 10.1
param.mc.sigma = 0.2;
param.mc.K = 0.5;
param.mc.tau = 0.005;
param.mc.b_MC = 0.5;
param.mc.A_PC = 0.1;
param.mc.A_BG = 1;
param.mc.A_PFC = 0.1;  
G = rand(param.mc.n, param.mc.n);
disp('CANN of MC Model runs ...');

% either this one
% [X, Y, G_new] = CANN(param, pc_som, pfc_som); 

% or this one
[G, G_all] = mcCANN(param.mc.n, param.mc.A_lat, param.mc.sigma, param.mc.K, param.mc.tau, param.mc.b_MC, G, param.mc.A_PC, pc_som, param.mc.A_BG, 0, param.mc.A_PFC, 0, param); % Eq. 10.11 & 10.12
G_all = reshape(G_all, 10000, []);
G_all = G_all';
disp('CANN of MC Model is computed');

%% Map MC output activity to MN
disp('SVM running: Mapping MC to MN');
% Input: G
% Predictor: phiMn
% Mapping to muscle length vector M_L
phiMN = d.muscleActivation;
aShoulderLabel = phiMN(:,1);
anShoulderLabel = phiMN(:,2);
aElbowLabel = phiMN(:,3);
anElbowLabel = phiMN(:,4);
labelStruct = {aShoulderLabel, anShoulderLabel, aElbowLabel, anElbowLabel};
svmModel = cell(4,1);
for i = 1:4
   svmModel{i} = fitrsvm(G_all(1:400,:), labelStruct{i});
end
disp('SVM finished: MC to MN mapped');
%% MN Model
% Set Path
%{
param.mn.A_MN = 0.1; % values in the book, page 183, Table 10.1
param.mn.mu_MC_MN = 0.1;
disp('MN Model runs ...');
phiMN = mn(param.mn.A_MN, param.mn.mu_MC_MN, param.mn.W_MC_MN, desiredActivation, networkActivation, G); % Eq. 10.13 % 10.14
disp('CANN of MC Model computed');
%}

%% Train the weight between PFC and MC, Eq. 10.25
%{
param.pfc.mu_PFC_MC = 0.1; % value in the book, page 183, Table 10.1
ksi = 1;    % End effector radius
pfcActivation = pfc_som.U;  % should be the output of SOM from PFC
W_PFC_MC = PFC_to_MC(G_targ, G_PFC, pfcActivation, param.pfc.mu_PFC_MC, endEffector, targetEffector, ksi);
% Equation 10.26 is missing, not sure where to implement
%}

%% Basal Ganglia Model
% Set Path
% Compute value computation and value difference, Eq. 10.15 & 10.16
disp('Basal Ganglia model runs...');
X_targ = rand(400,2);
param.bg.sigma_v = 2;
[V_arm, valueDifference] = valueComputation(param.bg.sigma_v, X_targ, d.pos, param.T); 
subplot(2,1,1);
bar(1:1:param.T, V_arm);
xlabel('Time');
ylabel('Magnitude');
title('The Unit Time Dependency of Value Computation')
subplot(2,1,2);
bar(1:1:param.T, valueDifference);
xlabel('Time');
ylabel('Magnitude');
title('The Unit Time Dependency of Value Difference')
% Striatum
% Compute the output of D1, Eq. 10.17 
param.bg.lamdaD1 = 50;
param.bg.tD1 = 0.02;
y_D1 = outputD1(param.bg.lamdaD1, param.bg.tD1, valueDifference, G_all, param);
% Compute the output of D2, Eq. 10.18 
param.bg.lamdaD2 = -50;
param.bg.tD2 = 0;
y_D2 = outputD2(param.bg.lamdaD2, param.bg.tD2, valueDifference, G_all, param);

% STN-GPe
% Compute dX_GPe/dt, ode45 solver is needed, Eq. 10.19
param.bg.epsilon_g = 1;  % value in the book, page 183, Table 10.1
param.bg.w_sg = 1;
param.bg.sigma_lat = 1;
mat = zeros(param.mc.n);
param.bg.A_ton = 3;
param.bg.lamda_ton = -50;
param.bg.theta_ton = 0.5;
param.bg.k = 9;
param.bg.epsilon_s = 1;
param.bg.w_gs = 1;
param.bg.tau_GPe = 0.005;
param.bg.tau_STN = 0.005;
param.bg.A_D1 = 15;
param.bg.A_D2 = 1;
param.bg.lamda_STN = 1; % no value is given for it in the book, controls to slope of 10.21
% center = given neuron [x_center, y_center]
center = [floor(param.mc.n/2) floor(param.mc.n/2)];
W_glat = gauss2d(mat, param.bg.sigma_lat, center);
y_STN = outputSTN(param.bg.lamda_STN, 0);
func = @(x_GPe) (1/param.bg.tau_GPe) * (-x_GPe + param.bg.w_sg*y_STN + param.bg.epsilon_g*sum(W_glat*x_GPe, 'All') + y_D2);
integralMin = 0;
integralMax = param.T;
x_GPe = integral(@(x_GPe) func(x_GPe), integralMin, integralMax, 'ArrayValued', true);

W_slat = W_glat;
func = @(x_STN) (1/param.bg.tau_STN)*(-x_STN + param.bg.w_gs*x_GPe + param.bg.epsilon_s*sum(W_slat*y_STN, 'All'));
x_STN = integral(@(x_STN) func(x_STN), integralMin, integralMax, 'ArrayValued', true);

% Output of STN
y_STN = outputSTN(param.bg.lamda_STN, x_STN); % Eq. 10.21

% STN-GPi
y_GPi = outputGPi(param.bg.A_D1, y_D1, param.bg.A_D2, y_STN); % Eq. 10.23

% Thalamus is being modeled as a CANN
param.thalamus.n = 10;
param.thalamus.sigma = 2; 
param.thalamus.A_w = 1;
param.thalamus.A_yGPi = 10;
param.thalamus.K = 0.5;
G_thalamus = rand(param.thalamus.n, param.thalamus.n);
I_ext = y_GPi';
I_ext = reshape(I_ext(:,end), param.thalamus.n^2, param.thalamus.n^2);
%BG = thalamus(param.thalamus.n, param.thalamus.sigma, param.thalamus.K, param.thalamus.A_w, param.T, y_GPi); % Last paragraph before 10.2.5 Prefrontal Cortex, page number = 177
%BG = thalamus(param.thalamus.n, param.thalamus.A_w, param.thalamus.sigma, param.thalamus.K, param.mc.tau, param.mc.b_MC, G_thalamus, param.thalamus.A_yGPi, y_GPi, param);
[X, Y, BG] = CANN(param, I_ext, 0);
surf(BG,'linestyle','none');
ylabel('Time');
xlabel('Node Index');
zlabel('Firing Rate');
title("Time Evolution of Firing Rates of Thalamus Neurons");
view(45,45);
disp('Basal Ganglia model is computed...');