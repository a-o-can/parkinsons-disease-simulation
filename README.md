# Parkinsons Disease Simulation

BSc. thesis repository for Ali Oguz Can

# Folders
* A Cortico-Basal Ganglia Model: This folder contains the code of the thesis.

* Attractor Neural Network: This folder contains code for an attractor neural network to simulate behaviours of different cortices.

* Hopfield: This folder contains utilies.

* OpenSim Code: This folder contains code for the software OpenSim to visually simulate the experiments.

* SVM: This folder contains code of a support-vector-machines model to simulate behaviours of different cortices.

* Self Organizing Map (SOM): This folder contains code of a SOM model to simulate behaviours of different cortices.

* Deprecated: This folder contains deprecated code.

# Files
* Bachelor_Thesis_SE.pdf: This is the feedback i received from my supervisor.

* Bachelor_Thesis.pdf: This is the latest version of the thesis.

* B.Sc._Ali_Oguz_Can_papers_revised.xlsx: A list of used literature.

* finalPresentation copy.pptx: Presentation used during the defense.
