import org.opensim.modeling.*;

%% Set a model and create the geometry
model = Model();
body = Body('body', 1.0, Vec3(0), Inertia(0)); % 1kg with (0,0,0) mass center
body.attachGeometry(Sphere(0.1));
model.addBody(body);

%% Adding the joint
joint = SliderJoint('joint', ... % Name
                    model.getGround(), ... % Parent PyhsicalFrame
                    body ...    % Child PyhsicalFrame
                    );
coord = joint.updCoordinate(); % a const reference to the coordinate associated with this joint
coord.setName('translation') % name of the coordinate in Coordinates Window
model.addJoint(joint);

%% Setting the actuator
actuator = CoordinateActuator('translation');
actuator.setName('actuator');
model.addForce(actuator);

%% Setting the controller
controller = PrescribedController();
controller.addActuator(actuator);
controller.prescribeControlForActuator('actuator', Sine());
model.addController(controller);

model.finalizeConnections();
model.print('pointmass.osim');

%% Simulate the model
model.setUseVisualizer(true);
initState = model.initSystem();
finalState = opensimSimulation.simulate(model, initState, 1.5);