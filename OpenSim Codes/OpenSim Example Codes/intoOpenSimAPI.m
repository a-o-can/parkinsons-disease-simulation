import org.opensim.modeling.*;

% Build a model.
model = Model();
body = Body('body', 1.0, Vec3(0), Inertia(0));
body.attachGeometry(Sphere(0.1));
model.addBody(body);

joint = SliderJoint('joint', model.getGround(), body);
coord = joint.updCoordinate();
coord.setName('translation');
model.addJoint(joint);

actuator = CoordinateActuator('translation');
actuator.setName('actuator');
model.addForce(actuator);

controller = PrescribedController();
% methodsview(controller);
controller.addActuator(actuator);
controller.prescribeControlForActuator('actuator', Sine());
model.addController(controller);

model.finalizeConnections();   
model.print('pointmass.osim');

% Simulate a model.
model.setUseVisualizer(true);
initState = model.initSystem();
disp(initState.getY());
finalState = opensimSimulation.simulate(model, initState, 1.5);

% Analyze a simulation.
disp(coord.getValue(finalState));
model.realizePosition(finalState);
disp(model.calcMassCenterPosition(finalState));
model.realizeAcceleration(finalState);
disp(joint.calcReactionOnParentExpressedInGround(finalState));