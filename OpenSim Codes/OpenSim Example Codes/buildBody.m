import org.opensim.modeling.*

% Build a Model
massCenter = Vec3(-0.0707, 0.0, 0.0);
inertia = Inertia(0.128, 0.0871, 0.0579, 0, 0, 0);
bodyName = 'pelvis';
bodyMass = 11;
newBody = Body(bodyName, bodyMass, massCenter, inertia);
