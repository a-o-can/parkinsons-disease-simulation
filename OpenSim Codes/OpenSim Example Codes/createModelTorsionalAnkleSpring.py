# This is section from the following example:
# https://simtk-confluence.stanford.edu:8443/display/OpenSim/Simulation-Based+Design+to+Reduce+Metabolic+Cost

# Description: In this section, we will create two models, each with a different simple assistive device on the
# right limb, using the scripting shell and Property Editor.

# Type in Command Prompt:

# Get a handle to the current model and create a new copy
baseModel = getCurrentModel()
ankleSpringModel = baseModel.clone()
ankleSpringModel.setName(baseModel.getName() + '_ankle_spring')

# Create the spring we'll add to the model (a CoordinateLimitForce in OpenSim)
ankleSpring = modeling.CoordinateLimitForce()
ankleSpring.setName('AnkleLimitSpringDamper')
# Set the coordinate for the spring
ankleSpring.set_coordinate('ankle_angle_r')

# Add the spring to the model
ankleSpringModel.addForce(ankleSpring)

# Load the model in the GUI
loadModel(ankleSpringModel)