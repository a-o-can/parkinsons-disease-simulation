import org.opensim.modeling.*;

% Create a Joint

bodyMass = 11;
massCenter = Vec3(-0.0707, 0.0, 0.0);
inertia = Inertia(0.128, 0.0871, 0.0579, 0, 0, 0);

% Parent Body
pBody = Body('pelvis', bodyMass, massCenter, inertia);

% Child Body
cBody = Body('femur_r', bodyMass, massCenter, inertia);

% Location of the joint origin expressed in the parent frame
locInParent = Vec3(-0.0707, -0.0661, 0.0835);
% Orientation of the joint frame in the parent frame
oriInParent = Vec3(0,0,0);

% Location of the joint origin expressed in the child frame
locInChild = Vec3(0,0,0);
% Orientation of the joint frame in the child frame
oriInChild = Vec3(0,0,0);

% Construct the hip joint
rHip = BallJoint('r_hip', pBody, locInParent, oriInParent, cBody, locInChild, oriInChild);

% Construct Model to Visualize
model = Model();
model.addBody(pBody);
model.addBody(cBody);
model.addJoint(rHip);

% Simualte
model.setUseVisualizer(true);
initState = model.initSystem();
disp(initState.getY());
finalState = opensimSimulation.simulate(model, initState, 1.5);