% Create a Torque Actuator for the Knee Joint (flex/ext)

import org.opensim.modeling.*;

myModel = Model('gait2354_scaled.osim');
femur_r = myModel.getBodySet().get('femur_r');
tibia_r = myModel.getBodySet().get('tibia_r');
zAxis = Vec3(0,0,1);
torqueActuator = TorqueActuator();
torqueActuator.setBodyA(femur_r);
torqueActuator.setBodyB(tibia_r);
torqueActuator.setAxis(zAxis);
torqueActuator.setOptimalForce(10);

myModel.setUseVisualizer(true);
initState = myModel.initSystem();
disp(initState.getY());
finalState = opensimSimulation.simulate(myModel, initState, 1.5);