import org.opensim.modeling.*

%% Load the model, joints and marker sets
model = Model('arm26.osim');

markerShoulder = model.getMarkerSet.get('r_acromion');
markerElbow = model.getMarkerSet.get('r_humerus_epicondyle');
markerWrist = model.getMarkerSet.get('r_radius_styloid');
jointOffset = model.getJointSet.get('offset');
jointShoulder = model.getJointSet.get('r_shoulder');
jointElbow = model.getJointSet.get('r_elbow');

model.finalizeConnections();

%% Configuration of the simulation
time = 7; % duration [sec]
sampleRate = 1000; % sample rate for muscle acutation [Hz]
nSample = time/sampleRate; % number of samples
reflexGain = 10; % reflex gain


%% Define muscle excitation
t = 1:0.1:time;
s1 = sin(t*pi*2*0.5)+1;
s2 = sin((t+0.5/0.5)*pi*2*0.5)+1;
excitation = zeros(length(t), model.getMuscles.getSize());
excitation(:,1) = s1*0.15; %fine sinusoid for 'BIClong'
excitation(:,2) = s1*0.1; %fine sinusoid for 'BICshort'
excitation(:,3) = s1*0.1; %fine sinusoid for 'BRA'
excitation(:,4) = s2*0.05; %fine sinusoid for 'TRIlat'
excitation(:,5) = s2*0.1; %fine sinusoid for 'TRIlong'
excitation(:,6) = s2*0.1; %fine sinusoid for 'TRImed'

%% Controllers
muscleController = PrescribedController();
reflex = ToyReflexController();

%% Prepare muscle actuation object
for i=1:model.getMuscles().getSize()
   muscleName = model.getMuscles().get(i-1);
   muscleController.addActuator(muscleName);
   muscleControlFunction = PiecewiseLinearFunction();
   for j = 1:length(t) % per actuation frame
       muscleControlFunction.addPoint(t(j),excitation(j,i)); % accumulate all the actuation values
   end
   muscleController.prescribeControlForActuator(string(model.getMuscles().get(i-1)), muscleControlFunction);
   reflex.addActuator(muscleName);
   reflex.set_gain(reflexGain)
end

%% Append the contro llers to the model
model.addController(muscleController);
model.addController(reflex);

%% Simulate the model
model.setUseVisualizer(true);
initState = model.initSystem();
finalState = opensimSimulation.simulate(model, initState, time);
model.print('anotherArm26.osim');