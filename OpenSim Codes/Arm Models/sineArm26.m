import org.opensim.modeling.*;

model = Model('arm26.osim');

%% Adding sinusoidal force
rHand  = model.getBodySet().get('r_ulna_radius_hand');
rShoulder = model.getBodySet().get('r_humerus');
forceHand = PrescribedForce('r_ulna_radius_hand', rHand);
forceShoulder = PrescribedForce('r_humerus', rHand);
forceHand.setName('handForce');
forceShoulder.setName('shoulderForce');
% Sine Signal with
% amplitude = 10,
% omega = 0.5,
% phase = 0,
% offset = 0.
forceHand.setForceFunctions(Sine(10,0.5,0,0), Sine(), Sine());
%forceShoulder.setForceFunctions(Sine(2,0.5,0,0), Sine(), Sine());
model.addForce(forceHand);
%model.addForce(forceShoulder);

%% Creating the .osim file
model.finalizeConnections();
model.print('sineArm26.osim');