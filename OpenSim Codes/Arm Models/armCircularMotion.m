import org.opensim.modeling.*

%% Load the model, be sure the model is in the same directory with the code.
model = Model('arm26.osim');
muscleSet = model.getMuscles();
TRIlong = muscleSet.get('TRIlong');
%TRIlat = muscleSet.get('TRIlat');
%TRImed = muscleSet.get('TRImed');
BIClong = muscleSet.get('BIClong');
BICshort = muscleSet.get('BICshort');
BRA = muscleSet.get('BRA');

%% Add a Controller with a Sine signal
controller = PrescribedController();
controller.addActuator(TRIlong);
controller.addActuator(TRIlat);
controller.addActuator(TRImed);
controller.addActuator(BIClong);
controller.addActuator(BICshort);
controller.addActuator(BRA);
% controller.addActuator(actuator2);
% Sine Signal with
% amplitude = 0.15,
% omega = 0.4
% phase = 0,
% offset = 0.
func = PiecewiseLinearFunction();
controller.prescribeControlForActuator('TRIlong', func);
%controller.prescribeControlForActuator('TRIlat', func);
%controller.prescribeControlForActuator('TRImed', func);
controller.prescribeControlForActuator('BIClong', func);
controller.prescribeControlForActuator('BICshort', func);
controller.prescribeControlForActuator('BRA', func);
model.addController(controller);

%% Finalize Connections
model.finalizeConnections();
model.print('armCircularMotion.osim');

%% 5 seconds of simulation time on MatLab GUI
model.setUseVisualizer(true);
initState = model.initSystem();
disp(initState.getY());
osimSimulate(model, initState, 5);
