%% Dynamic Neural Field Model (1D) from the book Fundamentals of Computational Neuroscience by Trappenberg

clf; hold on;
nn = 100; % number of neuron
dx = 2*pi/nn; % feature space
sig = 2*pi/10; % width of gaussian
C=0.5; % inhibition factor

%% Training weigth matrix
for loc = 1:nn
    i = (1:nn)';
    dis = min(abs(i-loc), nn - abs(i-loc));
    % The pattern matrix pat contains nn patterns, each is a Gaussian centred
    % around one of the nodes in the network
    pat(:,loc) = exp(-(dis*dx).^2 / (2*sig^2));
end
w = pat*pat'; % weigth matrix
w = w/w(1,1); % normalized weight matrix
w = 4*(w-C); % shifted and modulated weigth matrix

%% Start the experiment
tAll = []; rAll = []; % store the time values and the corresponding rate values of neurons

% Update with the localised input
I_Ext = zeros(nn,1); I_Ext(nn/2-floor(nn/10):nn/2+floor(nn/10)) = 1; % external input with non-zero elements for the 21 nodes around the central node
[t, u] = ode45('rnn_ode_u', [0 10], zeros(1,nn), [], nn, dx, w, I_Ext); % 
r = 1./(1+exp(-u));
tAll = [tAll; t]; rAll = [rAll; r];

% Update without the input
I_Ext = zeros(1,nn);
[t, u] = ode45('rnn_ode_u', [10 20], u(size(u,1), :), [], nn, dx, w, I_Ext); % 
r = 1./(1+exp(-u));
tAll = [tAll; t]; rAll = [rAll; r];

%% Plot the results
surf(tAll', 1:n, 'linestyle', 'none'); view(0, 90);