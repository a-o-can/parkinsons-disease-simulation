% Source: This is a code, which is found in the Paper 'Continuous Attractor
% Neural Networks' by Trappenberg (2003).

nTimeSteps = 10;
nNodes = 500;
% create random binary pattern between -1 and 1
pattern = 2*floor(2*rand(nNodes,nTimeSteps))-1;

% hebbian learning to update the weights of the fully connected network. 
w = pattern * pattern';

% initialize the model with a random vector, which has 500 nodes. The
% Network is slightly biased with the first training pattern: 0.1*pattern(:,1)
r=(2*rand(nNodes,1)-1) + 0.1*pattern(:,1);

% update the network by summing all the weighted inputs from the one
% previous time step. Transfer function is tanh(.).
for t = 2:nTimeSteps
    r(:,t) = tanh(w*r(:,t-1));
end

% plotting the results
plot(r'*pattern/nNodes);
xlabel("Time");
ylabel("Overlap");
title('Development of Evolution of the Network States');
legend

% Figure: Results from an ANN simulation. Each of the 10 lines in the plot
% correspond to the overlap (cosine distance) between the network state at each
% time step (abscissa) and a pattern that was used in the learning phase for
% training. The network state has initially only a small overlap with all of
% the stored patterns, but one of the stored patterns was retrieved completely
% at time step 9. This is an example with a rather long convergence time.
% Many other examples have much smaller convergence times.